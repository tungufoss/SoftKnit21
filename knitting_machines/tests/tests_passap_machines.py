    #!/usr/bin/env python
    # coding:utf-8
"""
      This file is part of SoftKnit21 framework

    License
    =======

     Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or  any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.



    Module purpose
    ==============

    Test Passap E6000 classes and functions

    Implements
    ==========



    Documentation
    =============


    Usage
    =====


    @author: Odile Troulet-Lambert
    @copyright: Odile Troulet-Lambert
    @license: AGPL
    """

import unittest


from SoftKnit21.commons.SoftKnit21Errors import *
from SoftKnit21.knitting_machines.concrete_machines.passap_machines import PassapE6000, Duomatic80
from SoftKnit21.knitting_machines.generic_machine import GNeedlesBed


class Test_machines(unittest.TestCase):

    def test_PassapE6000(self):
        self.assertEqual(len(PassapE6000.YarnMast), 4)
        self.assertEqual(PassapE6000.NeedlesBed[GNeedlesBed.BedName.FRONT_BED], PassapE6000.NeedlesBed[GNeedlesBed.BedName.BACK_BED])
        self.assertTrue('KX' in PassapE6000.CamCmd[GNeedlesBed.BedName.FRONT_BED][0].valueslist())
        self.assertFalse('KX' in PassapE6000.CamCmd[GNeedlesBed.BedName.BACK_BED][0].valueslist())
        self.assertTrue('AX' in PassapE6000.CamCmd[GNeedlesBed.BedName.BACK_BED][0].valueslist())
        self.assertEqual(PassapE6000.CamCmd[GNeedlesBed.BedName.BACK_BED][1].camValues[0].__class__.__name__, 'SelectionCamValue')

    def test_Duomatic80(self):
        self.assertEqual(Duomatic80.NeedlesBed[GNeedlesBed.BedName.FRONT_BED], PassapE6000.NeedlesBed[GNeedlesBed.BedName.BACK_BED])
        self.assertTrue('AX' in Duomatic80.CamCmd[GNeedlesBed.BedName.FRONT_BED][0].valueslist())
        self.assertTrue('AX' in Duomatic80.CamCmd[GNeedlesBed.BedName.BACK_BED][0].valueslist())
        self.assertEqual(Duomatic80.CamCmd[GNeedlesBed.BedName.BACK_BED][1].camValues[0].__class__.__name__, 'SelectionCamValue')


class TestNeedleBeds_validate_coding (unittest.TestCase):

    def setUp(self):

        self.tuplesNOK = (
            ('back_bed_N', '10101010'),
            ('back_bed_P', '11001100'),
            ('back_bed_PUW', '11110000'),
            ('front_bed_N', '1101010'),
            ('front_bed_P', '1001100'),
            ('front_bed_PUW', '1110000'),
        )
        self.tuplesOK = (
            ('back_bed_N', '11010'),
            ('back_bed_P', '10000'),
            ('back_bed_PUW', '11100'),
            ('front_bed_N', '11010'),
            ('front_bed_P', '11010'),
            ('front_bed_PUW', '11110')
        )
        self.short_names = (
            ('_N', '11010'),
            ('_P', '10000'),
            ('_PUW', '11100'),
        )

        self.tupleswrong_length = (
            ('back_bed_N', '11010'),
            ('back_bed_P', '100000'),
            ('back_bed_PUW', '11100'),
            ('front_bed_N', '11010'),
            ('front_bed_P', '11010'),
            ('front_bed_PUW', '11100')
        )

    def test_wrong_length(self):
        with self.assertRaises(ValidationError) as e:
            PassapE6000.NeedlesBed[GNeedlesBed.BedName.FRONT_BED].validate_coding('back_bed', self.tupleswrong_length)
        exc = e.exception
        self.assertTrue('length' in exc.args[0], msg=f'ex.args is {exc.args[0]}')
        self.assertTrue('back_bed' in exc.args[0], msg=f'exc.args is {exc.args[0]}')

    def test_tuples_OK(self):
        f = PassapE6000.NeedlesBed[GNeedlesBed.BedName.FRONT_BED].validate_coding('front_bed', self.tuplesOK)
        b = PassapE6000.NeedlesBed[GNeedlesBed.BedName.BACK_BED].validate_coding('back_bed', self.tuplesOK)
        self.assertTrue(f)
        self.assertTrue(b)

    def test_tuples_NOK(self):
        with self.assertRaises(ValidationError) as e:
            f = PassapE6000.NeedlesBed[GNeedlesBed.BedName.FRONT_BED].validate_coding('front_bed', self.tuplesNOK)
        exc = e.exception
        self.assertTrue('front_bed' in exc.args[0], msg=f'exc.args is {exc.args[0]}')
        self.assertTrue('PUW=0' in exc.args[0], msg=f'exc.args is {exc.args[0]}')
        with self.assertRaises(ValidationError) as e:
            b = PassapE6000.NeedlesBed[GNeedlesBed.BedName.BACK_BED].validate_coding('back_bed', self.tuplesNOK)
        exc = e.exception
        self.assertTrue('back_bed' in exc.args[0], msg=f'exc.args is {exc.args[0]}')
        self.assertTrue('PUW=1' in exc.args[0], msg=f'exc.args is {exc.args[0]}')


@ unittest.skip
class Test_numbering_schema(unittest.TestCase):

    def test_softknit_needle(self):
        """ tests conversion of position of needles" in softknit numbering"""
        self.assertEqual(self.nedbed.needleNumS(-90), 0)
        self.assertEqual(self.nedbed.needleNumS(89), 178)
        self.assertEqual(self.nedbed.needleNumS(-1), 89)
        self.assertEqual(self.nedbed.needleNumS(1), 90)
        with self.assertRaises(BedValueError):
            self.nedbed.needleNumS(0)

    def test_machine_needle(self):
        """ tests conversion of position of needles in machine numbering"""
        self.assertEqual(self.nedbed.needleNumM(0), -90)
        self.assertEqual(self.nedbed.needleNumM(89), -1)
        self.assertEqual(self.nedbed.needleNumM(90), 1)
        self.assertEqual(self.nedbed.needleNumM(91), 2)
        with self.assertRaises(BedValueError):
            self.nedbed.needleNumM(180)


if __name__ == '__main__':
    import unittest

    unittest.main()




