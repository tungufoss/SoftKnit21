# -*- coding: utf-8 -*-
""" This file is part of SoftKnit21 framework

License
=======

 - B{SofKnit21} (U{http://www.softkni21.org}) is Copyright:
  - (C) Odile.Troulet-Lambert@orange.fr

This program is free software; you can redistribute it and/or modify
it under the terms of the Affero GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Affero GNU General Public License for more details.

You should have received a copy of the Affero GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

software exceptions

Implements
==========

 - B{SoftKnit21Errors}

Documentation
=============

    
Usage
=====

@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""


class SoftKnit21Exception (Exception):
    """parent class of all exceptions of SoftKnit21 programm"""
    pass


class BadImplementationError (RuntimeError, SoftKnit21Exception):
    """ error in implementation classes which derive form ABC classes"""
    pass


class ValidationError (SoftKnit21Exception):
    """ error while validating data either from incoming data (models, fabrics) 
    or data resulting from user input"""


"""exceptions below this must be reviewed"""


class ModelStorageError (SoftKnit21Exception):
    """error in the reading the stored model"""
    pass


class KnitObjectError (SoftKnit21Exception):
    """ error KnitObject"""
    pass


class ConfigManagementValueError (SoftKnit21Exception):
    """ error settings """
    pass

#class E6000LockSettingsComputingError (Softknit21Exception):
    #""" error in the computing the lock settings """
    #pass


#class PusherManagementComputingError (Softknit21Exception):
    #""" error in the computing of pusher positions """
    #pass


#class GridPatternBinaryFuncNameError (Softknit21Exception):
    #""" error in computing binary functions name"""


#class MachineStateValueError (Softknit21Exception):
    #""" error in the state of the machine (pushers and needles) """
    #pass


#class StitchTechniqueError (Softknit21Exception):
    #""" error in the content of a StitchTechnique"""
    #pass


#class GridandStichError (Softknit21Exception):
    #""" error in the class GridandStitch"""
    #pass


#class GenFSMError (Softknit21Exception):
    #""" error ih the generic Finite State Machine Class"""


#class StitchSizeError (Softknit21Exception):
    #""" error ih the stitchSize object functions"""


#class LockPartValueError (Softknit21Exception):
    #"""  value error for the name of the LockPart"""


#class LockPosValueError (Softknit21Exception):
    #""" error in value for the Lock position"""


#class LockPosError (Softknit21Exception):
    #""" error in the lock position functions"""


#class BedValueError (Softknit21Exception):
    #""" error in values for the Bed"""


#class RackingValueError (Softknit21Exception):
    #""" error in reacking level """


#class PassapSpecificSettingValueError (Softknit21Exception):
    #""" error in PassapSpecifics """


#class PassapSpecificDicError (Softknit21Exception):
    #""" error in the dictionnary of lock settings"""


#class HelpersError (Softknit21Exception):
    #""" error in helpers function"""


#class StitchTechniqueStorageError (Softknit21Exception):
    #""" error in the storage of a StitchTechnique"""


#class LCMessageError (Softknit21Exception):
    #""" error in a LCmessage received or to send
    #to be caught in the lock_reader when create dic
    #or in the FSM when create message"""
    #pass


#class LCMessageConfError (LCMessageError):
    #"""error in a received message regarding configuration"""


#class LockControlerError (Softknit21Exception):
    #""" error in LockControler"""


#class FSMError (Softknit21Exception):
    #""" error in FSM"""


#class LCManagerError (Softknit21Exception):
    #"""error in the protocol Manager"""


