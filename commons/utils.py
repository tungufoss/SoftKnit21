#!/usr/bin/env python
# coding:utf-8
"""
  This file is part of SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

different classes and tools

Implements
==========


Documentation
=============


Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""

import enum
from SoftKnit21.commons.SoftKnit21Errors import BadImplementationError


class HEnum(str, enum.Enum):
    """an Enum class to define enumerations containing 3 attributes for each member: value, label and help"""

    def __new__(cls, value, label, help):
        obj = str.__new__(cls, value)
        obj._value_ = value
        obj.label = label
        obj.help = help
        return obj

    @classmethod
    def check_compatible(cls, other_enum, optional=None):
        """check that other_enum is compatible with self:
        compatible means all members of self are in other_enum except members listed in optional
        each member of other_enum has same code as self"""
        if optional:
                if not isinstance(optional, list):
                    optional = [optional]
                optional_keys = [op.name for op in optional]
        else:
            optional_keys = []
        for key, member in cls.__members__.items():
            m1 = f' {key} member of {cls.__qualname__} cannot be omited'

            try:
                assert ((key in other_enum.__members__.keys()) or (optional and key in optional_keys)), m1
                if not key in optional_keys:
                    m2 = f'{cls.__qualname__} member {key} cannot be overriden by {other_enum.__getattr__(key)}'
                    othermember = other_enum.__getattr__(key)
                    assert member.value == othermember.value, m2
                    assert member.name == othermember.name, m2
            except AssertionError as e:
                raise BadImplementationError(e.args) from e


if __name__ == '__main__':

    import unittest
    from unittest.mock import patch
    unittest.main()
