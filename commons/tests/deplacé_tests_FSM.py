#!/usr/bin/env python
# coding:utf-8
"""
  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

Tests for FSM class

Implements
==========

 - B{GridPattern}

Documentation
=============



Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""
from common.fsm import FSM
from common.SoftKnit21Errors import FSMError
import unittest
from unittest.mock import Mock, patch, call
import logging


class TestFSM(FSM):

    def _createTransitions(self):

        self._addTransition(event='a', src='idle', dst='1')
        self._addTransition(event='b', src='1', dst='2', )
        self._addTransition(event='c', src='idle', dst='2')
        self._addTransition(event='c', src='2', dst='3')
        self._addTransition(event='d', src='idle', dst='2', onAfter=[self._createSub, 'param2'])
        self._addTransition(event='dd', src='2', dst='1', onAfter=[self._onAfter, 'param1'])
        self._addTransition(event='e', src='idle', dst='1', onBefore=[self.return_cancel, 'cancel'])
        self._addTransition(event='ee', src='idle', dst='1', onBefore=self.return_cancel)

        self._addTransition(event='g', src='idle', dst=('1', '2'),
                            onCondition=self.test_condition,
                            onAfter=(None, [self._onAfter, 'param1']))
        self._addTransition(event='gg', src='idle', dst=('1', '2'),
                            onCondition=self.test_condition,
                            onAfter=([self._onAfter, 'param2'], None))
        self._addTransition(event='h', src='idle', dst='idle', onReEnter=self._onAfter)
        self._addTransition(event='hh', src='idle', dst='=', onLeave=self._createSub)
        self._addTransition(event='i', src='idle', dst='2', )
        self._addTransition(event='f', src='*', dst='1')
        self._addTransition(event='j', src='idle', dst='1', onLeave=self._onAfter_Error)

    def _onAfter(self, *args, **kwargs):
        print('onAfterOK')
        return

    def return_cancel(self, *args, **kwargs):
        if 'cancel' in args or 'cancel' in kwargs.keys():
            return 'cancel'
        else:
            return None

    def _onAfter_Error(self, *args, **kwargs):
        raise FSMError("An exception occured in _onAfter()")

    def test_condition(self, *args, **kwargs):
        if kwargs.get('r') == 0:
            return True
        else:
            return False

    def _createSub(self, *args, **kwargs):
        return


class FSMInit(unittest.TestCase):
    """Test add_transitions  dynamic construction of the fsm"""

    def setUp(self):
        self.fsmNoT = TestFSM()
        self.fsm = TestFSM(createTransitions=True)
        self.mockwritelog = Mock(spec=logging,
                                 wraps=logging,
                                 name='mockwritelog')
        #mlog.log(logging.DEBUG, 'in tests_LC_message_received')

    def tearDown(self):
        pass

    def test1_constructor_nodict(self):
        self.assertEqual(self.fsmNoT.state, 'idle')
        self.assertEqual(self.fsmNoT.name, 'main')
        fsm = TestFSM('standby', 'fsminit', createTransitions=True)
        self.assertEqual(fsm.state, 'standby')
        self.assertEqual(fsm.name, 'fsminit')
        # test duplicate transitions
        with self.assertRaises(FSMError):
            fsm._addTransition(event='a', src='idle', dst='1')
        fsm = TestFSM(triggerFirst='a', createTransitions=True)
        self.assertEqual(fsm.state, '1')

    def test2_trigger_normal(self):
        # normal transitions
        self.fsm.trigger('a')
        self.assertEqual(self.fsm.state, '1')
        self.fsm.trigger('b')
        self.assertEqual(self.fsm.state, '2')

    def test3_trigger_many_src(self):
        # test second transition in the dict of src
        self.fsm.trigger('c')
        self.assertEqual(self.fsm.state, '2')
        self.fsm.trigger('c')
        self.assertEqual(self.fsm.state, '3')

    def test_trigger_event_star(self):
        self.fsm._addTransition(event='*', src='idle', dst='2', onAfter=self.fsm._onAfter)
        self.fsm.trigger('k')
        self.assertEqual(self.fsm.state, '2')
        self.fsm.trigger('c')
        self.assertEqual(self.fsm.state, '3')

    def test4_trigger_log_errors(self):
        # no transition exist do not change state
        # or unknown event do not change state
        # in all 3 cases log
        with unittest.mock.patch('common.fsm.mlog', new=self.mockwritelog):
            self.fsm.trigger('z')
            self.assertEqual(self.fsm.state, 'idle')
            self.mockwritelog.log.assert_called_with(logging.CRITICAL, f'ignoring unknown or undefined event z for current state idle')
            self.fsm.trigger('a')
            self.fsm.trigger('c')
            self.mockwritelog.log.assert_called_with(logging.CRITICAL, f'ignoring unknown or undefined event c for current state 1')
            self.assertEqual(self.fsm.state, '1')

    def test5_trigger_cancel_event(self):
        # event cancel
        with unittest.mock.patch('common.fsm.mlog', new=self.mockwritelog):
            evName = 'e'
            state = 'idle'
            self.fsm.trigger('e')
            logmessage = call(10, f'canceling event {evName} current state {state}')
            self.assertEqual(self.fsm.state, 'idle')
            self.assertTrue(self.mockwritelog.log.call_args == logmessage)
            evName = 'ee'
            logmessage = call(10, f'canceling event {evName} current state {state}')
            self.fsm.trigger(evName, **{'cancel': 'True'})
            self.assertEqual(self.fsm.state, 'idle')
            self.assertTrue(self.mockwritelog.log.call_args == logmessage)


class TestCallbackswithmock (unittest.TestCase):
    """ tests for callbacks"""

    def setUp(self):
        patcher1 = patch('__main__.TestFSM._onAfter')
        self.mockonAfter = patcher1.start()
        self.addCleanup(patcher1.stop)

        patcher4 = patch('__main__.TestFSM._createSub')
        self.mockcreateSub = patcher4.start()
        self.addCleanup(patcher4.stop)

        self.fsm = TestFSM(createTransitions=True)

    def test6_trigger_callsin_ons(self):
        # test onAfter OK is called with event dictionnary
        self.fsm.trigger('d')
        self.mockcreateSub.assert_called_once_with('param2', evName='d', src='idle')
        self.assertEqual(self.fsm.state, '2')
        self.fsm.trigger('dd')
        self.mockonAfter.assert_called_once_with('param1', evName='dd', src='2')
        self.assertEqual(self.fsm.state, '1')

    def test7_trigger_reEnter(self):
        # dst is = src
        self.fsm.trigger('h')
        self.mockonAfter.assert_called_once_with(evName='h', src='idle')
        self.assertEqual(self.fsm.state, 'idle')
        self.fsm.trigger('hh')
        self.mockcreateSub.assert_called_once_with(evName='hh', src='idle')
        self.assertEqual(self.fsm.state, 'idle')

    def test8_trigger_onconditionTrue(self):
        # onCondition True
        self.fsm.trigger('g', r=0)
        self.mockonAfter.assert_called_once_with('param1', cond=True, evName='g', src='idle', r=0)
        self.assertEqual(self.fsm.state, '2')

    def test9_trigger_onconditionFalse(self):
        # oncondition False
        self.fsm.trigger('gg')
        self.mockonAfter.assert_called_once_with('param2', cond=False, evName='gg', src='idle')
        self.assertEqual(self.fsm.state, '1')


class TestCallErroronCondition (unittest.TestCase):
    """ tests for callbacks"""

    def setUp(self):
        self.fsm = TestFSM(createTransitions=True)

    def test10_trigger_raise_exception_in_callbacks(self):
        # raises exception in onAfterError
        with self.assertRaises(FSMError):
            self.fsm.trigger('j')
        self.assertEqual(self.fsm.state, 'idle')

    def test11_trigger_wildcards(self):
        # wildcard
        self.fsm.trigger('f')
        self.assertEqual(self.fsm.state, '1')
        self.fsm.trigger('f')
        self.assertEqual(self.fsm.state, '1')


class FSMInitDic(unittest.TestCase):
    # """ tests init with dictionary"""

    def testInit12(self):

        mainFsmTransitions = [{'event': 'a', 'src': 'start', 'dst': '1', },
                              {'event': 'a', 'src': '1', 'dst': '2', },

                              {'event': 'c', 'src': '2', 'dst': 'start', }, ]
        mainFsm = {'name': 'mainFsm', 'initialName': 'start', 'triggerFirst': None, 'transitions': mainFsmTransitions}

        self.fsm = FSM(initialName='start', name='mainFsm', createTransitions=True, transitions=mainFsmTransitions)
        self.assertEqual(self.fsm.state, 'start')
        self.fsm.trigger('a')
        self.assertEqual(self.fsm.state, '1')
        self.fsm.trigger('a')
        self.assertEqual(self.fsm.state, '2')
        self.fsm.trigger('c')
        self.assertEqual(self.fsm.state, 'start')


if __name__ == '__main__':

    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__file__', test=True)

    unittest.main()
