"""
  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

tests for configuration Management

Implements
==========

 - B{test for configuration manager}

Documentation
=============
 tests for the module configuration manager
Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""
from common.configuration_manager import LockControlerConf, PathsConfig, Configuration, ConfElement, get_configuration
from common.configuration_manager import _get_testConfigFileName, _get_testBaseDir, get_test_configuration
from common.SoftKnit21Errors import ConfigManagementValueError
import unittest

from pathlib import Path
import logging

mlog = logging.getLogger(__name__)


class TestConfigurationClass (unittest.TestCase):
    """test init of Configuration object
    uses a test configuration path so as not to destroy existing real configuration
    """
    import pickle
    from unittest.mock import patch

    def setUp(self):
        self.conf = Configuration()
        """ depending on where the test program is launched BaseDir is different
        configs pickle file read and generated in tests are not real ones"""

        # test configuration file location
        self.test_config_file = _get_testConfigFileName()

    def test_init(self):
        self.assertFalse(self.conf.saved)
        self.assertTrue(isinstance(self.conf.lockControlerConf, LockControlerConf))
        self.assertTrue(isinstance(self.conf.pathsConfig, PathsConfig))

    def test_saveConfig_new(self):

        import pickle

        with unittest.mock.patch('common.configuration_manager._get_configFileName', return_value=self.test_config_file):
            self.conf.save()
            self.assertTrue(self.conf.saved)
            ex = Path(self.test_config_file).exists()
            self.assertTrue(ex)
            # checks that can be read
            with open(self.test_config_file, 'rb') as f:
                configuration = pickle.load(f)
        self.assertTrue(isinstance(configuration, Configuration))

    def tearDown(self):
        p = _get_testConfigFileName()
        ex = Path(p).exists()
        if ex:
            Path(p).unlink()


class TestConfigurationElement (unittest.TestCase):
    """test configuration Element"""

    def setUp(self):
        self.c = Configuration()
        self.cel = ConfElement(self.c)
        self.testBaseDir = _get_testBaseDir()

    def test__init(self):
        self.assertTrue(self.c is self.cel.globalconf)

    def test_update(self):
        with unittest.mock.patch('common.configuration_manager._get_baseDir', return_value=self.testBaseDir):
            with unittest.mock.patch.object(self.cel, 'update') as mock:
                self.cel.update()
                mock.assert_called_once()
            self.cel.update()
            self.assertFalse(self.cel.default)

    def tearDown(self):
        p = _get_testConfigFileName()
        ex = Path(p).exists()
        if ex:
            Path(p).unlink()


class TestLockControlerConf(unittest.TestCase):
    """ tests for LockControlerGonfig method"""

    def setUp(self):
        # create configuration if it does not exist
        self.conf = Configuration()
        self.lcc = LockControlerConf(self.conf)
        self.test_config_file = _get_testConfigFileName()

    def test_init(self):
        self.assertTrue(isinstance(self.lcc, LockControlerConf))
        self.assertTrue(self.lcc.globalconf is self.conf)

    def test_fill(self):
        with unittest.mock.patch('common.configuration_manager._get_configFileName',
                                 return_value=self.test_config_file):
            self.lcc.fill({'val1': 1}, val2=2, val3=3)
            self.assertEqual(self.lcc.val1, 1)
            self.assertEqual(self.lcc.val2, 2)
            self.assertEqual(self.lcc.val3, 3)
            with unittest.mock.patch.object(self.lcc, 'fill') as mock:
                self.lcc.fill({'val1': 1}, val2=2, val3=3)
                mock.assert_called_once()

    def test_getLockConfElement(self):
        with unittest.mock.patch('common.configuration_manager._get_configFileName',
                                 return_value=self.test_config_file):

            self.lcc.fill({'val1': 1}, val2=2, val3=3)
            self.assertEqual(self.lcc.get_element('val1'), 1)
            self.assertEqual(self.lcc.get_element('toto'), None)

    def tearDown(self):
        p = _get_testConfigFileName()
        ex = Path(p).exists()
        if ex:
            Path(p).unlink()


class TestPathConfig(unittest.TestCase):
    """ tests for PathConfig class"""

    def setUp(self):
        self.testBaseDir = _get_testBaseDir()
        self.test_config_file = _get_testConfigFileName()
        self.test_baseDirData = Path(_get_testBaseDir() / 'data')

        p = _get_testConfigFileName()
        ex = Path(p).exists()
        if ex:
            Path(p).unlink()
        self.conf = Configuration()
        with unittest.mock.patch('common.configuration_manager._get_baseDir', return_value=self.testBaseDir):
            self.cp = PathsConfig(self.conf)

    def test_ini(self):
        self.assertTrue(isinstance(self.cp, PathsConfig))
        self.assertEqual(self.test_baseDirData / 'stitchtech.sqlite3', self.cp.stitchTech)
        self.assertEqual(self.test_baseDirData / 'gridpatterns', self.cp.gridPatterns)
        self.assertTrue(self.cp.default)
        self.assertTrue(self.cp.globalconf is self.conf)
        self.assertTrue(self.cp.default)

    def test_getGridPatternDir(self):
        self.assertEqual(self.cp.getGridPatternsDir('E6000'), self.test_baseDirData / 'gridpatterns' / 'E6000_gridpatterns')
        self.assertEqual(self.cp.getGridPatternsDir('E8000'), self.test_baseDirData / 'gridpatterns' / 'E8000_gridpatterns')
        with self.assertRaises(ConfigManagementValueError):
            self.cp.getGridPatternsDir('toto')

    def test_updatePathsConfig(self):
        with unittest.mock.patch('common.configuration_manager._get_baseDir', return_value=self.testBaseDir):
            self.cp.update()
            self.assertFalse(self.cp.default)
            with unittest.mock.patch.object(self.cp, 'update') as mock:
                self.cp.update()
                mock.assert_called_once()

    def tearDown(self):
        p = _get_testConfigFileName()
        ex = Path(p).exists()
        if ex:
            Path(p).unlink()


class TestGetConfiguration (unittest.TestCase):
    from unittest.mock import patch

    def setUp(self):
        self.test_base_dir = _get_testBaseDir()
        self.test_config_file = _get_testConfigFileName()

    def test_check_global_variable(self):
        with unittest.mock.patch('common.configuration_manager._get_configFileName',
                                 return_value=self.test_config_file):
            conf = get_configuration()
            self.assertIsInstance(conf, Configuration)
            with unittest.mock.patch('common.configuration_manager.mlog',
                                     autospec=logging.Logger,
                                     wraps=logging.Logger) as mocklog:
                conf = get_configuration()
                (level, lmessage), emptydic = mocklog.log.call_args_list[0]
                self.assertEqual(level, logging.DEBUG)
                self.assertEqual(lmessage, 'configuration already loaded')

    def test_configuration_file_exists_global_var_None(self):
        with unittest.mock.patch('common.configuration_manager._get_configFileName',
                                 return_value=self.test_config_file):
            conf = Configuration()
            conf.save()
            with unittest.mock.patch('common.configuration_manager._configuration', None):
                with unittest.mock.patch('common.configuration_manager.mlog',
                                         autospec=logging.Logger,
                                         wraps=logging.Logger) as mocklog:
                    configuration = get_configuration()
                    (level, lmessage), emptydic = mocklog.log.call_args_list[0]
                    self.assertEqual(level, logging.DEBUG)
                    self.assertEqual(lmessage, f'reading existing configuration')
                    self.assertTrue(isinstance(configuration, Configuration))
                    # we should compare configuration and conf for one conf element
                    # do the comparison for the pathsconfig element but exclude the attribute globalconfiguration
                    dic1 = vars(conf.pathsConfig)
                    dic1.update({'globalconf': None})
                    dic2 = vars(configuration.pathsConfig)
                    dic2.update({'globalconf': None})
                    self.assertDictEqual(dic1, dic2)
                    self.assertTrue(isinstance(configuration, Configuration))

        # delete the file
        p = _get_testConfigFileName()
        ex = Path(p).exists()
        if ex:
            Path(p).unlink()

    def test_configuration_file_does_not_exist_global_var_None(self):
        # delete the file
        p = _get_testConfigFileName()
        ex = Path(p).exists()
        if ex:
            Path(p).unlink()
        with unittest.mock.patch('common.configuration_manager._get_configFileName',
                                 return_value=self.test_config_file):
            with unittest.mock.patch('common.configuration_manager' + '._configuration', None):
                with unittest.mock.patch('common.configuration_manager.mlog',
                                         autospec=logging.Logger,
                                         wraps=logging.Logger) as mocklog:
                    configuration = get_configuration()
                    (level, lmessage), emptydic = mocklog.log.call_args_list[0]
                    self.assertEqual(level, logging.DEBUG)
                    self.assertEqual(lmessage, f'no configuration exists, creating default one')
                    self.assertTrue(isinstance(configuration, Configuration))
                    ex = Path(self.test_config_file).exists()
                    self.assertTrue(ex)

        p = _get_testConfigFileName()
        ex = Path(p).exists()
        if ex:
            Path(p).unlink()

    def test_create_test_config_get_normal_config(self):

        conf1 = get_test_configuration()
        conf2 = get_configuration()
        self.assertEqual(conf1, conf2)

    def tearDown(self):
        p = _get_testConfigFileName()
        ex = Path(p).exists()
        if ex:
            Path(p).unlink()


if __name__ == '__main__':
    import unittest
    from common.tests.tests_FSM import FSMInit, TestCallbackswithmock, TestCallErroronCondition, FSMInitDic, TestFSM
    from common.tools_for_log import logconf

    mlog = logconf('__file__', test=True)

    unittest.main()
