#!/usr/bin/env python
# coding:utf-8
"""  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

creates environment for XML encoding

Implements
==========


Documentation
=============


Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""
from pathlib import Path
import xmlschema as xs
import sys


from SoftKnit21.commons.tools_for_log import logconf
import generic_knitting_model.helpers as h

mlog = logconf('__name__', test=True)


class DKpartModelConverter (xs.DataBindingConverter):

    def element_decode(self,
                       data: xs.ElementData,
                       xsd_element: 'XsdElement',
                       xsd_type=None,
                       level: int = 0):
        data_element = super().element_decode(data, xsd_element, xsd_type, level)
        obj = xsd_element.binding.xml_to_python(data_element)
        data_element.obj = obj
        return data_element


def get_element_parents(root, element):
    """returns the list of parents of element so as to locate the element in the xml code"""
    import importlib
    module = importlib.import_module('Ludd21.generic_knitting_model.kpart_model')
    dic = module.__dict__
    classes = [dic[c] for c in dic if (isinstance(dic[c], type) and dic[c].__module__ == module.__name__)]

    def get_class(tag):
        """returns the class corresponding to a tag"""
        return next((c for c in classes if c.__name__ == capitalize(tag)))

    possible_classes = get_class(element.local_name)

    print(element.attrib)


if __name__ == '__main__':
    import unittest
    from commons.tools_for_log import logconf

    mlog = logconf('__name__', test=True)
    from pathlib import Path
    current = Path(__file__)
    tests_dir = Path(current.parent, 'tests')
    loader = unittest.TestLoader()
    tests_suite = loader.discover(tests_dir, pattern='tests_' + current.stem + '*.py')
    testRunner = unittest.runner.TextTestRunner()
    testRunner.run(tests_suite)






