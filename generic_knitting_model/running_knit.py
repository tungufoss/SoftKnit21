
"""
  This file is part of SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL

"""
#from knitting_model.grid_pattern import GridPattern
#from knitting_model.stitch_technique import StitchTechnique


class RunningKnit ():
    """ RunningKnit is an instance of a swatched knitting piece which is being knitted on a knitting machine
    It includes additionnal information related to which parts where already knitted. 
    This class is to be completed
    """

    def __init__(self):
        """Constructor"""

    def nextCP(self):
        """ returns the next CP in the form of
        leftmost, rightmost, buffer
        buffer contains pushers values for the positions between letmost and rightmost"""

        return

    # def start(self, side):
        #"""start current work and returns the side of the lock to start on"""
        # return

    def check_end_pass(self, side):
        """returns True if  the side of the lock correspond to the 'R' or 'L' message
        
        @att side : message received 
        @type : string 'L' or 'R' """

        return

    def endCP(self, side):
        """ end of carriage pass update visualisation, no return"""
        pass

    def knit_reset(self):
        """ returns the type of buffer message to send 'l' or 'r'"""
        return

    def terminate_current_work(self):
        """ reset of lock controler requires saving curent work"""


if __name__ == '__main__':

    # from tests. import
    c = RunningKnit(148, 40, '1008')
    c.set_numpos(40)
    c.initPUW(41)
    print('cest fini')
    # unittest.main()
