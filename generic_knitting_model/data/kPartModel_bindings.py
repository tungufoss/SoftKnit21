import xmlschema as xs
from xmlschema.dataobjects import DataElement, DataBindingMeta
import generic_knitting_model.helpers as h
import generic_knitting_model.kpart_model as kpm


"""
XML decode data bindings for schema kPartModel.xsd
"""

schema_path = h.get_schema_path(kpm.KPartModel)

__NAMESPACE__ = "http://www.ludd21.com/KPartModel"


schema = xs.XMLSchema11(schema_path)





class LineborderBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['lineBorder']

    @staticmethod
    def xml_to_python(dataelement):
        child = list(dataelement)[0]
        obj = kpm.LineBorder (**child.obj)
        return obj

class HorincreaseborderBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['horIncreaseBorder']

    @staticmethod
    def xml_to_python(dataelement):
        child = list(dataelement)[0]
        obj = kpm.HorIncreaseBorder (**child.obj)
        return obj

class HordecreaseborderBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['horDecreaseBorder']

    @staticmethod
    def xml_to_python(dataelement):
        child = list(dataelement)[0]
        obj = kpm.HorDecreaseBorder (**child.obj)
        return obj

class HorjoinborderBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['horJoinBorder']

    @staticmethod
    def xml_to_python(dataelement):
        child = list(dataelement)[0]
        obj = kpm.HorJoinBorder (**child.obj)
        return obj

class HorholdborderBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['horHoldBorder']

    @staticmethod
    def xml_to_python(dataelement):
        child = list(dataelement)[0]
        obj = kpm.HorHoldBorder (**child.obj)
        return obj

class VerticalborderBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['verticalBorder']

    @staticmethod
    def xml_to_python(dataelement):
        child = list(dataelement)[0]
        obj = kpm.VerticalBorder (**child.obj)
        return obj

class ArcborderBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['arcBorder']

    @staticmethod
    def xml_to_python(dataelement):
        child = list(dataelement)[0]
        obj = kpm.ArcBorder (**child.obj)
        return obj

class CubicbezierborderBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['cubicBezierBorder']

    @staticmethod
    def xml_to_python(dataelement):
        child = list(dataelement)[0]
        obj = kpm.CubicBezierBorder (**child.obj)
        return obj

class QuadraticbezierborderBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['quadraticBezierBorder']

    @staticmethod
    def xml_to_python(dataelement):
        child = list(dataelement)[0]
        obj = kpm.QuadraticBezierBorder (**child.obj)
        return obj




class CastonpartsegBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['castOnPartSeg']
    
    @staticmethod
    def xml_to_python(dataelement):
        d = {ele.obj.__class__.__name__: ele.obj for ele in list(dataelement)}
        d.update(dataelement.attrib)
        obj = kpm.CastOnPartSeg(**d)
        return obj

class CastonpartpointBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['castOnPartPoint']
    
    @staticmethod
    def xml_to_python(dataelement):
        d = {ele.obj.__class__.__name__: ele.obj for ele in list(dataelement)}
        d.update(dataelement.attrib)
        obj = kpm.CastOnPartPoint(**d)
        return obj

class CastoffpartsegBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['castOffPartSeg']
    
    @staticmethod
    def xml_to_python(dataelement):
        d = {ele.obj.__class__.__name__: ele.obj for ele in list(dataelement)}
        d.update(dataelement.attrib)
        obj = kpm.CastOffPartSeg(**d)
        return obj

class CastoffpartpointBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['castOffPartPoint']
    
    @staticmethod
    def xml_to_python(dataelement):
        d = {ele.obj.__class__.__name__: ele.obj for ele in list(dataelement)}
        d.update(dataelement.attrib)
        obj = kpm.CastOffPartPoint(**d)
        return obj

class JoinpartBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['joinPart']
    
    @staticmethod
    def xml_to_python(dataelement):
        d = {ele.obj.__class__.__name__: ele.obj for ele in list(dataelement)}
        d.update(dataelement.attrib)
        obj = kpm.JoinPart(**d)
        return obj

class SplitpartBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['splitPart']
    
    @staticmethod
    def xml_to_python(dataelement):
        d = {ele.obj.__class__.__name__: ele.obj for ele in list(dataelement)}
        d.update(dataelement.attrib)
        obj = kpm.SplitPart(**d)
        return obj

class SegsplitpartBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['segSplitPart']
    
    @staticmethod
    def xml_to_python(dataelement):
        d = {ele.obj.__class__.__name__: ele.obj for ele in list(dataelement)}
        d.update(dataelement.attrib)
        obj = kpm.SegSplitPart(**d)
        return obj





class KppieceslistBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['kpPiecesList']
    
    @staticmethod
    def xml_to_python(dataelement):
        l = [ele.obj for ele in list(dataelement)]
        try : 
            obj = kpm.KpPiecesList(l)
            return obj
        except AttributeError :
            key = 'kpPiecesList'
            d = {key : l}
            return d

class KpartslistBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['kPartsList']
    
    @staticmethod
    def xml_to_python(dataelement):
        l = [ele.obj for ele in list(dataelement)]
        try : 
            obj = kpm.KPartsList(l)
            return obj
        except AttributeError :
            key = 'kPartsList'
            d = {key : l}
            return d

class LeftborderslistBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['leftBordersList']
    
    @staticmethod
    def xml_to_python(dataelement):
        l = [ele.obj for ele in list(dataelement)]
        try : 
            obj = kpm.LeftBordersList(l)
            return obj
        except AttributeError :
            key = 'leftBordersList'
            d = {key : l}
            return d

class RightborderslistBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['rightBordersList']
    
    @staticmethod
    def xml_to_python(dataelement):
        l = [ele.obj for ele in list(dataelement)]
        try : 
            obj = kpm.RightBordersList(l)
            return obj
        except AttributeError :
            key = 'rightBordersList'
            d = {key : l}
            return d




class KpartmodelBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['kPartModel']
    
    @staticmethod
    def xml_to_python(dataelement):
        attrs = convert_complex_attributes(dataelement)
        for child in list(dataelement):
            attrs.update(child.obj)
        obj = kpm.KPartModel(**attrs)
        return obj

class KppieceBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['kpPiece']
    
    @staticmethod
    def xml_to_python(dataelement):
        attrs = convert_complex_attributes(dataelement)
        for child in list(dataelement):
            attrs.update(child.obj)
        obj = kpm.KpPiece(**attrs)
        return obj

class BasepointBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['basePoint']
    
    @staticmethod
    def xml_to_python(dataelement):
        attrs = convert_complex_attributes(dataelement)
        for child in list(dataelement):
            attrs.update(child.obj)
        obj = kpm.BasePoint(**attrs)
        return obj

class BasesegBinding(DataElement, metaclass=DataBindingMeta):
    xsd_element = schema.elements['baseSeg']
    
    @staticmethod
    def xml_to_python(dataelement):
        attrs = convert_complex_attributes(dataelement)
        for child in list(dataelement):
            attrs.update(child.obj)
        obj = kpm.BaseSeg(**attrs)
        return obj


def convert_complex_attributes(dataelement):
    new = dataelement
    attgroup = dataelement.xsd_type.attributes
    for att in attgroup.iter_components():
        if att.local_name:
            att_value = dataelement.attrib[att.local_name]
            if att.local_name and att.type.local_name == "point":
                new.attrib[att.local_name] = complex(att_value[0], att_value[1])
            else:
                new.attrib[att.local_name] = att_value
    return new.attrib
    
class SegmentBinding(DataElement,
                     #metaclass=DataBindingMeta
                     ):
    @staticmethod
    def xml_to_python(dataelement):
        """ convert the values of attributes whose type is complex
        returns a dictionnary of attributes"""
        obj = convert_complex_attributes(dataelement)
        return obj

