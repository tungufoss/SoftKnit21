#!/usr/bin/env python
# coding:utf-8
"""  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

tests for configuration Management

Implements
==========

 - B{test for configuration manager}

Documentation
=============
 tests for the module configuration manager
Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""

from pathlib import Path

#from generic_knitting_model.kpart_model import KpPiece, KPart, SegSplitPart, CastOffPart, SplitPart

import unittest
import cmath


__all__ = [
]


class Test_super_class (unittest.TestCase):

    def get_tests_datadir(self, callingfile, common=False):
        """ returns the PosixPath for the data file for the tests_script"""

        script = Path(callingfile).stem
        callingfile = Path(callingfile)
        if 'tests' in callingfile.parts:
            #filepath = Path(*callingfile.parts[0:-1], 'data', script)
            filepath = Path(callingfile.parent, 'data')
        else:
            #filepath = Path(*callingfile.parts[0:-1], 'tests', 'data', script)
            filepath = Path(callingfile.parent, 'tests', 'data')
        if common:
            return Path(filepath, 'xml')
        else:
            return Path(filepath, script)

    #def print_kparts_iterable(self, callingfile, kPartsList, output):
        #""" create and save a file containing previous parts and nexts parts as python list"""

        #datadir = self.get_tests_datadir(self, callingfile)
        #stem = Path(callingfile).stem
        #if not Path(datadir, 'partsiterables').exists():
            #Path(datadir, 'partsiterables').mkdir()
        #pypath = Path(datadir, 'partsiterables', output + '.py')
        #if not pypath.exists():
            #pypath.touch()
        #with open(pypath, 'w') as fpp:

            #def genprevious(kp):
                #for p in kp.previousnum:
                    #yield(f'{p.partNumber},')

            #def gennexts(kp):
                #for p in kp.nextsnum:
                    #yield (f'{p.partNumber},')

            #for kp in kPartsList:
                #if kp.previousnum:
                    #p = genprevious(kp)
                    #line = f"""part{kp.partNumber}.previous =  [ {' '.join(list(p))} ]'\n' """
                    #fpp.write(line)
                #if kp.nextsnum:
                    #n = gennexts(kp)
                    #line = f"""part{kp.partNumber}.nexts =  [ {' '.join(list(n))} ]'\n' """
                    #fpp.write(line)




