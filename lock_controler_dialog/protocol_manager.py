# !/usr/bin/env python
# coding:utf-8
"""
  This file is part of SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

LCProtocol Manager managing the communication protocol with the LC

Implements
==========

 LCProtocolMgr

Documentation
=============

  LCProtocolMgr implements 4 FSMs : MainFSM, InitFsm, KnitFsm, TestFsm
Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""

from lock_controler_dialog.fsm import FSM


from lock_controler_dialog.LC_Helpers import LCProtocolHelpers, LCConfigurations
from common.tools_for_log import StrucLogMessage
from common.SoftKnit21Errors import LCManagerError, LockControlerError
from common.configuration_manager import get_configuration
from lock_controler_dialog.LC_message_tosend import LCMessagetoSend
from lock_controler_dialog.LC_messages_manager import LCMesManager
from gettext import gettext as _


import logging


mlog = logging.getLogger(__name__)


class InitFSM (FSM):
    """ subFSM for initialisation phase
    attributes of FSM class
    see documentation for the graphic description of transitions
    """

    def _createTransitions(self):
        """timeout expires do not trigger transitions ; they are implemented in
        the write_repeat of the lock_writer controler. if action on timeout is not to repeat,
        then a transition exist."""

        self._addTransition(event='on_msg_in_w', src='waitingLC', dst='hardwareCheck',
                            onBefore=self.parent.timer_cancel, onEnter=self.parent.LCconnectionOK,
                            onAfter=[self.parent.create_write_message, 'D', LCProtocolHelpers.TIMEOUT_MESSAGE])

        self._addTransition(event='on_max_repeat', src='waitingLC', dst='=',
                            onAfter=self.parent.LCconnectionNOK)

        self._addTransition(event='on_LC_connection_failed', src='waitingLC', dst='stop',
                            onAfter=self.parent.stop)

        self._addTransition(event='on_msg_in_d', src='hardwareCheck', dst=('watingUsrHard', 'LCReady'),
                            onBefore=self.parent.timer_cancel,
                            onCondition=self.parent.hardwareOK,
                            onAfter=([self.parent.call_presenter, 'request',
                                      _(f'turn on Lock Controler power supply'),
                                      'on_usr_click_hard'], None))

        self._addTransition(event='on_usr_click_hard', src='watingUsrHard', dst='hardwareCheck',
                            onEnter=[self.parent.create_write_message, 'D', LCProtocolHelpers.TIMEOUT_MESSAGE])

        self._addTransition(event='on_int_start_R', src='LCready', dst='waitingUserLockPosR',
                            onAfter=[self.parent.call_presenter, 'request',
                                     _(f'position lock on right in Start Position'),
                                     'on_usr_click_start'])

        self._addTransition(event='on_int_start_L', src='LCready', dst='waitingUserLockPosL',
                            onAfter=[self.parent.call_presenter, 'request',
                                     _(f'position lock on left in Start Position'),
                                     'on_usr_click_start'])

        self._addTransition(event='on_usr_click_start', src='waitingUserLockPosR',
                            dst='waitingLCAckR',
                            onAfter=[self.parent.create_write_message, 'Y',
                                     LCProtocolHelpers.TIMEOUT_MESSAGE])

        self._addTransition(event='on_usr_click_start', src='waitingUserLockPosL', dst='waitingLCAckL',
                            onAfter=[self.parent.create_write_message, 'Z', LCProtocolHelpers.TIMEOUT_MESSAGE])

        self._addTransition(event='on_msg_in_N', src='waitingLCAckR', dst='waitingUserLockPosR',
                            onBefore=self.parent.timer_cancel,
                            onAfter=[self.parent.call_presenter, 'request',
                                     _(f'position lock on right in Start Position'),
                                     'on_usr_click_start'])

        self._addTransition(event='on_msg_in_N', src='waitingLCAckL', dst='waitingUserLockPosL',
                            onBefore=self.parent.timer_cancel,
                            onAfter=[self.parent.call_presenter, 'request',
                                     _(f'position lock on left in Start Position'),
                                     'on_usr_click_start'])

        self._addTransition(event='on_msg_in_A', src='waitingLCAckR', dst='',
                            onBefore=self.parent.timer_cancel,
                            onAfter=[self.parent.changeFsm, 'MainFsm', 'readyKnitTest'])

        self._addTransition(event='on_msg_in_A', src='waitingLCAckL', dst='',
                            onBefore=self.parent.timer_cancel,
                            onAfter=[self.parent.changeFsm, 'MainFsm', 'readyKnitTest'])

        self._addTransition(event='on_max_repeat', src='*', dst='stop',
                            onAfter=self.parent.stop)


class KnitFSM (FSM):
    """ sub fsm for knit states
    see documentation for graphical description of KnitFsm
    """

    def _createTransitions(self):
        """timeout expires do not trigger transitions ; they are implemented in
        the write_repeat of the lock_writer controler. if action on timeout is not to repeat,
        then a transition exist. """

        # transitions from 'knit state'

        self._addTransition(event='on_msg_in_a', src='knit', dst='=',
                            onLeave=self.parent.onAck)

        self._addTransition(event='on_msg_in_n', src='knit', dst='=',
                            onBefore=self.parent.timer_cancel, onLeave=self.parent.onNack)

        self._addTransition(event='on_msg_in_R', src='knit', dst=('waitingUserConf', 'knit'),
                            onBefore=self.parent.timer_cancel,
                            onCondition=[self.parent.check_side, 'R'],
                            onAfter=([self.parent.call_presenter, 'request',
                                      'lock, thread, pushers',
                                      'on_usr_click_lock_thread_push'],
                                     self.parent.end_CP))

        self._addTransition(event='on_msg_in_L', src='knit', dst=('waitingUserConf', 'knit'),
                            onBefore=self.parent.timer_cancel,
                            onCondition=[self.parent.check_side, 'L'],
                            onAfter=([self.parent.call_presenter, 'request',
                                      'lock, thread, pushers',
                                      'on_usr_click_lock_thread_push'],
                                     self.parent.end_CP))

        self._addTransition(event='on_msg_in_e_50', src='knit', dst='waitingUserConf',
                            onBefore=self.parent.timer_cancel,
                            onAfter=[self.parent.call_presenter, 'request',
                                     'lock, thread, pushers',
                                     'on_usr_click_lock_thread_push'])

        # transitions from waitinguserconf
        self._addTransition(event='on_usr_click_lock_thread_push', src='waitingUserConf',
                            dst=('knit', 'knitreset'),
                            onCondition=[lambda **dic: dic.get('reset')],
                            onAfter=(None, self.parent.write_Reset))

        self._addTransition(event='on_msg_in_a', src='waitingUserConf', dst='=',
                            onBefore=self.parent.timer_cancel, onAfter=self.parent.onAck)

        self._addTransition(event='on_msg_in_n', src='waitingUserConf', dst='=',
                            onBefore=self.parent.timer_cancel, onAfter=self.parent.onNack)

        self._addTransition(event='on_msg_in_L', src='waitingUserConf', dst='=',
                            onBefore=self.parent.timer_cancel)
        self._addTransition(event='on_msg_in_R', src='waitingUserConf', dst='=',
                            onBefore=self.parent.timer_cancel)
        self._addTransition(event='on_msg_in_e_50', src='waitingUserConf', dst='=',
                            onBefore=self.parent.timer_cancel)

        # transitions from knitreset

        self._addTransition(event='on_msg_in_a', src='knitreset', dst='knit',
                            onBefore=self.parent.timer_cancel, onAfter=self.parent.onAck)

        self._addTransition(event='on_msg_in_e_50', src='knitreset', dst='waitingUserConf',
                            onAfter=[self.parent.call_presenter, 'request',
                                     'lock, thread, pushers',
                                     'on_usr_click_lock_thread_push'])

        self._addTransition(event='*', src='knitreset', dst='=', onBefore=self.parent.timer_cancel)

        # error transitions from any source state
        self._addTransition(event='on_msg_in_w', src='*', dst='init',
                            onBefore=self.parent.timer_cancel,
                            onLeave=[self.parent.call_presenter, 'message',
                                     _(f'an error occured in the lock controler, please save the work'),
                                     None, ],
                            onAfter=[self.parent.LC_reset])

        self._addTransition(event='on_usr_click_knit_reset', src='*', dst='knitreset',
                            onAfter=self.parent.write_Reset)

        self._addTransition(event='on_msg_in_e_1*', src='*', dst='',
                            onBefore=self.parent.timer_cancel,
                            onLeave=[self.parent.call_presenter, 'message',
                                     _(f'rail seems to have dust save work and clean the rail'),
                                     None],
                            onAfter=self.parent.LC_reset)

        self._addTransition(event='on_msg_in_e_20', src='*', dst='=',
                            onReEnter=[self.parent.logevent, logging.DEBUG, 'user warning speed'],
                            onLeave=[self.parent.call_presenter, 'message',
                                     _(f'lock seems to move to fast'),
                                     None])

        self._addTransition(event='on_max_repeat', src='*', dst='stop',
                            onAfter=self.parent.stop)

        self._addTransition(event='on_msg_in_d', src='*', dst='=',
                            onLeave=[self.parent.logevent, logging.DEBUG, 'dump from LC'])

        self._addTransition(event='on_msg_in_test', src='*', dst='=',
                            onLeave=[self.parent.logevent, logging.DEBUG, 'dump from LC'])

        # error transitions from knitreset

        self._addTransition(event='on_msg_in_e_3*', src='knitreset', dst='waitingUserConf',
                            onBefore=self.parent.timer_cancel,
                            onLeave=[self.parent.logevent, logging.CRITICAL, 'error in buffers transmission'],
                            onEnter=[self.parent.create_write_message, 'K', LCProtocolHelpers.TIMEOUT_MESSAGE],
                            onAfter=[self.parent.call_presenter, 'request',
                                     'lock, thread, pushers',
                                     'on_usr_click_lock_thread_push'])

        self._addTransition(event='on_msg_in_e_4*', src='knitreset', dst='=',
                            onAfter=[self.parent.errorBufferMess, False])

        # error transitions from knit

        self._addTransition(event='on_msg_in_e_4*', src='knit', dst='=',
                            onAfter=self.parent.errorBufferMess)

        self._addTransition(event='on_msg_in_e_3*', src='knit', dst='waitingUserConf',
                            onBefore=self.parent.timer_cancel,
                            onLeave=[self.parent.logevent, logging.CRITICAL, 'error in buffers while knitting'],
                            onEnter=[self.parent.create_write_message, 'K', LCProtocolHelpers.TIMEOUT_MESSAGE],
                            onAfter=[self.parent.call_presenter,
                                     'request', 'lock, thread, pushers',
                                     'on_usr_click_lock_thread_push'])

        # error transitions from waitinguserconf
        self._addTransition(event='on_msg_in_e_3*', src='waitingUserConf', dst='=',
                            onBefore=self.parent.timer_cancel,
                            onLeave=[self.parent.logevent, logging.CRITICAL, 'error in buffers while knitting'])

        self._addTransition(event='on_msg_in_e_4*', src='waitingUserConf', dst='=',
                            onAfter=[self.parent.errorBufferMess, False])


class TestsFSM (FSM):
    """subFSM for tests"""
    pass


class LCProtocolMgr (FSM):
    """ manages the states of the 4 FSMs and create LCMessagetoSend for the LC

    @att LC : reference of the lock controler
    @type : LockControler

    @_currentFsm : current fsm
    @type : FSM

    @initFsm : initFsm instance
    @type : fsm

    @knitFsm : knitFsm instance
    @type : fsm

    @testFsm : testFsm instance
    @type : fsm

    @messageNumber : message number of the message currently being sent
    @type : int

    @mesRepeat : number of repeat for the message to be sent or string of the event to trigger on time out expire
    @type : int

    @currentMess : last sent message
    @type : LCMessagetoSend

    @LCBuffers : number of full buffers in the LC
    @ type : int

    @running_knit : curent_work being knitted
    @type : instance of CurrentWork
    
    @queue : the events queue used by lockReader to write events and by protocol manager to read the events
    @type : an instance of a python simple QueueObject 
    """

    @property
    def currentFsm(self):
        return self._currentFsm

    @currentFsm.setter
    def currentFsm(self, value):
        raise LCManagerError(f'to change current Fsm use the ChangeFsm method')

    def __init__(self, queue):
        """Constructor"""
        self.queue = queue
        self.LC = LCMesManager(self, self.queue)
        self._currentFsm = self
        self.MainFsm = self
        # to handle messageNumbers and checksumErrors
        self.messageNumber = 0
        self.currentMess = None
        self.LCConnection_is_OK = False

        # to handle number of buffers available in the LC
        self.LCBuffers = 0
        # do not create transitions now because some method need the other fsms
        self.InitFsm = InitFSM(initialName='waitingLC', name='InitFsm',
                               parent=self, createTransitions=False)
        self.KnitFsm = KnitFSM(initialName='knit', name='KnitFsm',
                               parent=self, createTransitions=False)
        self.TestsFsm = TestsFSM(initialName='idle', name='TestsFsm',
                                 parent=self, createTransitions=False)
        super().__init__(initialName='start', name='MainFsm', parent=None, createTransitions=True)
        self.InitFsm._createTransitions()
        self.KnitFsm._createTransitions()
        # self.TestsFsm._createTransitions()

    def configure_presenter(self, presenter):
        """ configures the presenter"""

        self.pres = presenter

    def configure_running_knit(self, running_knit):
        """configures the running_knit"""

        self.running_knit = running_knit

    def _createTransitions(self):
        """ creates Transisitions for the MainFsm"""

        self._addTransition(event='on_start', src='start', dst='',
                            onAfter=[self.changeFsm, 'InitFsm', 'waitingLC'],
                            onLeave=[self.create_write_message, 'W', LCProtocolHelpers.TIMEOUT_MESSAGE])

        self._addTransition(event='on_user_click_knit', src='readyKnitTest', dst='',
                            onAfter=[self.changeFsm, 'KnitFsm', 'knit'],
                            onLeave=self.nextBuffer)

        self._addTransition(event='on_usr_click_test', src='readyKnitTest', dst='',
                            onAfter=[self.changeFsm, 'TestsFsm'])

        self._addTransition(event='', src='stop', dst='')

    def hardwareOK(self, **event):
        """ read hardware parameter in configuration and returns True if hardware parameters are OK"""
        conf = get_configuration()
        brand = conf.lockControlerConf.get_element('brand')
        return LCConfigurations.is_hardware_OK(brand, **event)

    def check_mtype(self, messmtype, **event):
        """ returns True if mtype of message in event == mtype """
        if event['mtype'] == messmtype:
            return True
        else:
            return False

    def timer_cancel(self, **event):
        """ cancels the timer on sent message"""
        if self.currentMess is not None:
            timer = self.currentMess.timer
            if timer is not None:
                timer.cancel()
                self.currentMess.timer = None

    def _startSubFsm(self, name, initial=None, **event):
        sub = getattr(self, name)
        self._currentFsm = sub
        if initial is not None:
            states = (state for key, dic in self.currentFsm._transitions.items() for state in dic.keys())
            if initial not in states:
                raise LCManagerError('state {} does not exist in {}'.format(initial, self._currentFsm))
        else:
            initial = sub._initialState
        sub.set_state(initial)
        mlog.log(logging.DEBUG,
                 'FSM : {} started ; state: {}'.format(self.currentFsm, self.currentFsm.state))

    def changeFsm(self, start, initial=None, **event):
        """ changes the fsm and log the changes ;
        if the event is of type on_msg_in the event is already logged
        @param start : the name of the FSM to start
        @start type :  an instance of FSM

        @param initial : target state for the started FSM
        @type :  string"""

        mlog.log(logging.DEBUG,
                 'change of Fsm close FSM: {} start FSM: {}'.format(self.currentFsm, start))
        self._startSubFsm(start, initial, **event)

    def create_write_message(self, LCmtype, timeout=None, **event):
        """create and write single byte LCMessage
        if event is of type 'on-msg_in' do not log is was logged in the reception of message"""
        self.timer_cancel()
        self.currentMess = LCMessagetoSend(**{'mtype': LCmtype})
        self.LC.write_message(self.currentMess, timeout)
        dic = self.currentMess.toDict()
        dic.update({'message': 'message sent :'})
        mlog.log(logging.DEBUG, StrucLogMessage(**dic))

    def onAck(self, **event):
        """ preceding message x or l or r is acknowledged"""
        self.timer_cancel()
        mtype = self.currentMess.mtype
        if mtype != 'l' and mtype != 'r' and mtype != 'x':
            raise LCManagerError(
                f'onAck called for a message received which is not of type l nor r nor x')
        mesnum = event.get('mesNumber')
        currentnum = self.currentMess.datadict['mesNumber']
        if mesnum == currentnum:
                self.nextBuffer()
                self.currentMess = None
        else:
            mlog.log(logging.CRITICAL,
                     (f'Ack received does not correspond to current message whose number is {currentnum}'))

    def onNack(self, **event):
        """ preceding message l or r has a checksum error"""
        self.timer_cancel()
        mtype = self.currentMess.mtype
        if mtype != 'l' and mtype != 'r':
            raise LCManagerError(
                f'onNAck called for a message received of type {mtype} which is not of type l nor r ')
        mesnum = event.get('mesNumber')
        currentnum = self.currentMess.datadict['mesNumber']
        if (mesnum == currentnum
                and ((self.currentMess.mtype == 'l')
                 or (self.currentMess.mtype == 'r'))):
            self.LC.repeat_write(self.currentMess, LCProtocolHelpers.TIMEOUT_MESSAGE)
            mlog.log(logging.DEBUG,
                     f' negative acknowledgment for message {mesnum} type {mtype}, message resent')

        else:
            mlog.log(logging.CRITICAL,
                     f'NAck received does not correspond to current message whose number is {currentnum}')

    def logevent(self, level, message, **event):
        """creates a structured message with event and logs it"""
        event.update({'message': message})
        mlog.log(level, StrucLogMessage(**event))

    def errorBufferMess(self, repeat=True, **event):
        """ an error of type 4* occured try to repeat the message if repeat = True
        and mesNumber corresponds to last l or r message"""
        self.timer_cancel()
        mesnum = event.get('mesNumber')
        if repeat == False:
            mlog.log(logging.CRITICAL, f'error 4* not in knit state, do not repeat')
            return
        if (mesnum == self.currentMess.datadict['mesNumber']
                and ((self.currentMess.mtype == 'l') or
                 (self.currentMess.mtype == 'r'))):
            self.LC.repeat_write(self.currentMess, LCProtocolHelpers.TIMEOUT_MESSAGE)
            mlog.log(logging.CRITICAL, f'error 4* for message {mesnum}, message resent')
        else:
            mlog.log(logging.CRITICAL, f'error 4* for unknown message')

    def nextBuffer(self, **event):
        """ prepares the sending of the next buffer to the LC"""

        if self.LCBuffers < LCProtocolHelpers.NB_MAX_BUFFERS:
            self.messageNumber = (self.messageNumber + 1) % 128
            leftmost, rightmost, buffer, side = self.running_knit.nextCP()
            buffKey = 'LTRBuffer' if side == 'l' else 'RTLBuffer'
            self.currentMess = LCMessagetoSend(**{
                'mtype': side,
                'mesNumber': self.messageNumber,
                'leftmost': leftmost,
                'rightmost': rightmost,
                buffKey: buffer})
            self.LC.write_message(self.currentMess, LCProtocolHelpers.TIMEOUT_MESSAGE)
            self.LCBuffers += 1
            dic = self.currentMess.toDict()
            dic.update({'message': 'NextBuffer sent'})
            mlog.log(logging.DEBUG, StrucLogMessage(**dic))

    def write_Reset(self, **event):
        """ cancels timer, sends an x message if not already done"""
        self.timer_cancel()
        self.messageNumber = (self.messageNumber + 1) % 128
        self.LCBuffers = 0

        self.currentMess = LCMessagetoSend(
            ** {'mtype': 'x',
                'bufftype': self.running_knit.knit_reset(),
                'mesNumber': self.messageNumber})
        self.LC.write_message(self.currentMess, LCProtocolHelpers.TIMEOUT_MESSAGE)
        dic = self.currentMess.toDict()
        dic.update({'message': 'write_reset'})
        mlog.log(logging.CRITICAL, StrucLogMessage(**dic))

    def end_CP(self, **event):
        """ notify the running_knit about end of carriage pass
        get new buffer"""
        side = event.get('mtype')
        if side == 'R' or side == 'L':
            self.running_knit.endCP(side)
        else:
            raise LCManagerError(f'end_CP called with mtype {side}; should be R or L'
                                 )
        self.LCBuffers -= 1
        self.nextBuffer()
        mlog.log(logging.DEBUG, f'end_CP side = {side}')

    def LC_reset(self, **event):
        """ send a reset to the lock controler by sending a W message"""
        self.running_knit.terminate_current_work()
        mlog.log(logging.CRITICAL, f'LC_reset')
        self.changeFsm('InitFsm', 'waitingLC', **event)
        self.create_write_message('W', **event)

    def stop(self, **event):
        """ stops communication with the LC """
        self.running_knit.terminate_current_work()
        mlog.log(logging.CRITICAL, f'fatal failure of communication with the lock controler')
        self.changeFsm('MainFsm', 'stop', **event)
        # to de here dump LC to write in log
        self.call_presenter('message', _(f'a fatal error occured Lock Controler'
                                         f'is going to be reset, please save current work'), None)

    def trigger(self, **event):
        """ trigger the transition of the current Fsm"""
        evName = event.pop('evName')
        if evName == None:
            mlog.log(logging.CRITICAL, 'trigger for an event whose name is None')
        if self.currentFsm == self:
            super().trigger(evName, **event)
        else:
            self.currentFsm.trigger(evName, **event)

    def check_side(self, side, **event):
        """ checks end of row (R or L) corresponds to the direction of the current row"""

        return self.running_knit.check_end_pass(side)

    def LCconnectionOK(self, **event):
        """ save the lock controler configuration
        to be done set flag for continuation of main
        Note brand is valid; has been checked in LCMessage checkbrand"""
        portparamsdic = self.LC.portparams
        LCbrand = event.get('brand')
        LCofthisbrand = [dic for dic in LCConfigurations.KNOWN_LCs if dic.get('brand') == LCbrand]
        portparamsdic.update({'hard': event.get('hard'),
                              'soft': event.get('soft')})
        portparamsdic.update(LCofthisbrand[0])
        conf = get_configuration()
        conf.lockControlerConf.fill(**portparamsdic)
        conf.lockControlerConf.update()
        self.LCConnection_is_OK = True

    def LCconnectionNOK(self, **event):
        """ look for another LC if it exists
        in case the device chosen is not really a lock controler"""
        try:
            self.timer_cancel()
            self.LC.retry_LC_connection()
            self.LC.write_message(self.currentMess,
                                  timeout=LCProtocolHelpers.TIMEOUT_MESSAGE)
            dic = self.currentMess.toDict()
            dic.update({'message': 'try another USB connection sending:'})
            mlog.log(logging.DEBUG, StrucLogMessage(**dic))
        except LockControlerError:
            event.update({'evName': 'on_LC_connection_failed'})
            self.trigger(**event)

    def call_presenter(self, req, message, callback, blocking=False, **event):
        """ calls the presenter """
        if req == 'request':
            self.pres.usr_request(message, callback, blocking)
        if req == 'message':
            self.pres.usr_msg_out(message)


if __name__ == '__main__':
    """ this code loads all tests modules related to this module"""

    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__name__', test=True)

    from pathlib import Path
    current = Path(__file__)
    tests_dir = Path(current.parent, 'tests')
    loader = unittest.TestLoader()
    tests_suite = loader.discover(tests_dir, pattern='tests_' + current.stem + '*.py')
    testRunner = unittest.runner.TextTestRunner()
    testRunner.run(tests_suite)
