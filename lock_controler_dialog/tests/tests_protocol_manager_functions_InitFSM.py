#!/usr/bin/env python
# coding:utf-8
"""
  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

Test for class LCProtocolMgr : second part of the tests for the methods

Implements
==========

Documentation
=============

Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""
import unittest

from lock_controler_dialog.LC_message_tosend import LCMessagetoSend
from unittest.mock import Mock, patch
from lock_controler_dialog.tests.tests_super_class import Test_super_class
from common.SoftKnit21Errors import LockControlerError
from lock_controler_dialog.LC_Helpers import LCProtocolHelpers
import logging
import json


class Test_functions_InitFSM (Test_super_class, unittest.TestCase):

    def setUp(self):

        super().commonsetUp_for_pm()
        self.conf = super().get_test_configuration(__file__, forceNew=True)

    def test1_LCconnectionOK(self):
        """ checks LCconfiguration OK and saved"""

        event = {'evName': 'on_msg_i_w', 'brand': 'PassapE6000 ', 'soft': '1.0', 'hard': '2.0'}
        dataportparams = {'param1': 'value', 'param2': 'value'}
        self.pm.LC.portparams = dataportparams

        self.pm.LCconnectionOK(**event)

        event.update(dataportparams)
        event.pop('evName')
        LCconfdic = {key: value for (key, value)
                     in self.conf.lockControlerConf.__dict__.items()
                     if key in event.keys()}

        self.assertDictEqual(LCconfdic, event)

    def test2_LCConnectionNOK_retry_succesfull(self):
        """test same state, write-message called, log called
        """
        self.pm.LC.retry_LC_connection = Mock()
        messdic = {'mtype': 'W'}
        self.pm.currentMess = LCMessagetoSend(**messdic)
        event = {'evName': 'on_max_repeat'}

        self.pm.LCconnectionNOK(**event)

        args, kwargs = self.pm.LC.write_message.call_args
        (message, ) = args
        self.assertEqual(message, self.pm.currentMess)
        self.assertEqual(kwargs.get('timeout'), LCProtocolHelpers.TIMEOUT_MESSAGE)
        (level, evdic), emptydic = self.mocklog.log.call_args_list[0]
        self.assertEqual(level, logging.DEBUG)
        logdic = json.loads(str(evdic))
        re = {key: value for key, value in logdic.items() if (key, value) in self.pm.currentMess.toDict().items()}
        self.assertEqual(re, self.pm.currentMess.toDict())

    def test3_LCConnectionNOK_retry_unsuccesfull(self):
        """ test new event created and trigger"""
        self.pm.LC.retry_LC_connection = Mock(side_effect=LockControlerError)
        eventin = {'evName': 'on_max_repeat'}
        with patch.object(self.pm.currentFsm, 'trigger',
                          autospec=self.pm.currentFsm.trigger):

            self.pm.LCconnectionNOK(**eventin)

            arg, event = self.pm.currentFsm.trigger.call_args_list[0]
            self.assertEqual(event.get('evName'), 'on_LC_connection_failed')

    def test4_hardwareOK(self):
        """ test reads configuration and call hardwareOK"""
        eventOK = {'evName': 'on_msg_in_d', 'V15': 14, 'Vcc': 5}
        eventNOK = {'evName': 'on_msg_in_d', 'V15': 13, 'Vcc': 5}
        conf = super().get_test_configuration(__file__, forceNew=True)
        # note default lockControlerConf is empty
        # need brand
        dic = {'brand': 'PassapE6000 '}
        conf.lockControlerConf.fill(**dic)
        conf.lockControlerConf.update()
        self.assertTrue(self.pm.hardwareOK(**eventOK))
        self.assertFalse(self.pm.hardwareOK(**eventNOK))

    def test5_stop(self):
        """test stop log critical and changeFsm to MainFsm stop"""

        event = {'evName': 'on_msg_in_e_4*'}

        self.pm.stop(**event)

        self.pm.running_knit.terminate_current_work.assert_called_once()
        (level, message), emptydic = self.mocklog.log.call_args_list[0]
        self.assertEqual(level, logging.CRITICAL)
        self.assertEqual(self.pm.currentFsm, self.pm.MainFsm)
        self.assertEqual(self.pm.currentFsm.state, 'stop')


if __name__ == '__main__':
    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__file__', test=True)
    unittest.main()


