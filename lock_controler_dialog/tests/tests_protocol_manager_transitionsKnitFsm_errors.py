#!/usr/bin/env python
# coding:utf-8
"""
  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

Tests for the protocol manager : tests for the knitFsm transitions from knit state
Implements
==========

 - B{TestProtocolManager}

Documentation
=============



Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""
import unittest

from lock_controler_dialog.LC_message_tosend import LCMessagetoSend
from lock_controler_dialog.LC_message_received import create_from_dic
from common.tools_for_log import search_in_log

import logging
from lock_controler_dialog.tests.tests_super_class import Test_super_class


class TestKnitFsmTransitions_errors_from_any_source_state (Test_super_class, unittest.TestCase):
    """ tests for KnitFsm transitions with src = star
    test objective only go through each transition and check new state
    other tests suites are targeted towards testing the results of the functions """

    def setUp(self):
        """ TestMainFsmTransitions creates the protocol manager instance and do the appropriate mocks
        changes current Fsm to KnitFsm and go to knit_reset
        """

        super().commonsetUp_for_pm()

        event = {'evName': 'evènement'}
        self.pm.changeFsm('KnitFsm', initial='knitreset', **event)
        self.pm.pres.usr_request.reset_mock()
        self.pm.pres.usr_msg_out.reset_mock()

    def test_on_msg_in_w(self):

        messagedic = self.dataformessages.get('mw')
        message = create_from_dic(messagedic)
        event = message.createEvent()

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm, self.pm.InitFsm)
        self.assertEqual(self.pm.currentFsm.state, 'waitingLC')
        self.pm.pres.usr_msg_out.assert_called_once()
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, 'LC_reset',
                                      logging.CRITICAL))

    def test_on_usr_click_knit_reset(self):
        self.pm.messageNumber = 10
        event = {'evName': 'on_usr_click_knit_reset'}
        attrs = {'knit_reset.return_value': 'l'}
        self.pm.running_knit.configure_mock(**attrs)

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, 'knitreset')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, 'write_reset',
                                      logging.CRITICAL))

    def test_on_msg_in_e_1star(self):

        dicmessage = self.dataformessages.get('me')
        message = create_from_dic(dicmessage)
        event = message.createEvent()
        event.update({'errorcode': 11, 'evName': 'on_msg_in_e_1*'})

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm, self.pm.InitFsm)
        self.assertEqual(self.pm.currentFsm.state, 'waitingLC')

        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, 'LC_reset',
                                      logging.CRITICAL))

    def test_on_msg_in_e_20(self):

        dicmessage = self.dataformessages.get('me')
        message = create_from_dic(dicmessage)
        event = message.createEvent()
        event.update({'errorcode': 20, 'evName': 'on_msg_in_e_20'})
        currentstate = self.pm.currentFsm.state

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm, self.pm.KnitFsm)
        self.assertEqual(self.pm.currentFsm.state, currentstate)
        self.pm.pres.usr_msg_out.assert_called_once()
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test_on_max_repeat(self):

        event = {'evName': 'on_max_repeat'}

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm, self.pm)
        self.assertEqual(self.pm.currentFsm.state, 'stop')
        self.pm.pres.usr_msg_out.assert_called_once()
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, 'failure',
                                      logging.CRITICAL))

    def test_on_msg_in_d(self):

        event = {'evName': 'on_msg_in_d', 'bufftype': 'l'}
        currentstate = self.pm.currentFsm.state

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, currentstate)
        self.assertEqual(self.pm.currentFsm, self.pm.KnitFsm)
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, 'dump from LC',
                                      logging.DEBUG))

    def test_on_msg_in_test(self):

        event = {'evName': 'on_msg_in_test', 'bufftype': 'l'}
        currentstate = self.pm.currentFsm.state

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, currentstate)
        self.assertEqual(self.pm.currentFsm, self.pm.KnitFsm)
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, 'dump from LC',
                                      logging.DEBUG))


class TestKnitFsmTransitions_errors_from_knit_reset (Test_super_class, unittest.TestCase):
    """ tests for KnitFsm transitions with src = star
    test objective only go through each transition and check new state
    other tests suites are targeted towards testing the results of the functions """

    def setUp(self):
        """ TestMainFsmTransitions creates the protocol manager instance and do the appropriate mocks
        changes current Fsm to KnitFsm and go to knit_reset
        """

        super().commonsetUp_for_pm()

        event = {'evName': 'evènement'}
        self.pm.changeFsm('KnitFsm', initial='knitreset', **event)

    def test_on_msg_in_e_3_star(self):

        dicmessage = self.dataformessages.get('me')
        message = create_from_dic(dicmessage)
        event = message.createEvent()
        event.update({'errorcode': 30, 'evName': 'on_msg_in_e_3*'})

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm, self.pm.KnitFsm)
        self.assertEqual(self.pm.currentFsm.state, 'waitingUserConf')
        self.pm.pres.usr_request.assert_called_once()
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list,
                                      'error in buffers transmission',
                                      logging.CRITICAL))

    def test_on_msg_in_e_4_star(self):

        self.pm.currentMess = LCMessagetoSend(**self.dictmessages.get('ml1'))
        messnum = self.pm.currentMess.datadict.get('mesNumber')
        dicmessage = self.dataformessages.get('me')
        message = create_from_dic(dicmessage)
        event = message.createEvent()
        event.update({'errorcode': 40, 'evName': 'on_msg_in_e_4*', 'mesNumber': messnum})
        currentstate = self.pm.currentFsm.state

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm, self.pm.KnitFsm)
        self.assertEqual(self.pm.currentFsm.state, currentstate)
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, 'error 4*',
                                      logging.CRITICAL))


class TestKnitFsmTransitions_errors_from_knit (Test_super_class, unittest.TestCase):
    """ tests for KnitFsm transitions with src = WatingUserConf
    test objective only go through each transition and check new state
    other tests suites are targeted towards testing the results of the functions """

    def setUp(self):
        """ TestMainFsmTransitions creates the protocol manager instance and do the appropriate mocks
        changes current Fsm to KnitFsm
        """

        super().commonsetUp_for_pm()

        event = {'evName': 'evènement'}
        self.pm.changeFsm('KnitFsm', initial='knit', **event)

    def test_on_msg_in_e_4_star(self):

        self.pm.currentMess = LCMessagetoSend(**self.dictmessages.get('ml1'))
        messnum = self.pm.currentMess.datadict.get('mesNumber')
        dicmessage = self.dataformessages.get('me')
        message = create_from_dic(dicmessage)
        event = message.createEvent()
        event.update({'errorcode': 40, 'evName': 'on_msg_in_e_4*', 'mesNumber': messnum})
        currentstate = self.pm.currentFsm.state

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm, self.pm.KnitFsm)
        self.assertEqual(self.pm.currentFsm.state, currentstate)
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list,
                                      'error 4*',
                                      logging.CRITICAL))

    def test_on_msg_in_e_3_star(self):

        dicmessage = self.dataformessages.get('me')
        message = create_from_dic(dicmessage)
        event = message.createEvent()
        event.update({'errorcode': 30, 'evName': 'on_msg_in_e_3*'})

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm, self.pm.KnitFsm)
        self.assertEqual(self.pm.currentFsm.state, 'waitingUserConf')
        self.pm.pres.usr_request.assert_called_once()
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list,
                                      'error in buffers while knitting',
                                      logging.CRITICAL))

    def tearDown(self):
        pass


class TestKnitFsmTransitions_errors_from_waitinguserconf (Test_super_class, unittest.TestCase):
    """ tests for KnitFsm transitions with src = waitinguserconf
    test objective only go through each transition and check new state
    other tests suites are targeted towards testing the results of the functions """

    def setUp(self):
        """ TestMainFsmTransitions creates the protocol manager instance and do the appropriate mocks
        changes current Fsm to KnitFsm"""

        super().commonsetUp_for_pm()

        event = {'evName': 'evènement'}
        self.pm.changeFsm('KnitFsm', initial='waitingUserConf', **event)

    def test_on_msg_in_e_3_star(self):

        dicmessage = self.dataformessages.get('me')
        message = create_from_dic(dicmessage)
        event = message.createEvent()
        event.update({'errorcode': 30, 'evName': 'on_msg_in_e_3*'})

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm, self.pm.KnitFsm)
        self.assertEqual(self.pm.currentFsm.state, 'waitingUserConf')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list,
                                      'error in buffers while knitting',
                                      logging.CRITICAL))

    def test_on_msg_in_e_4_star(self):

        self.pm.currentMess = LCMessagetoSend(**self.dictmessages.get('ml1'))
        messnum = self.pm.currentMess.datadict.get('mesNumber')
        dicmessage = self.dataformessages.get('me')
        message = create_from_dic(dicmessage)
        event = message.createEvent()
        event.update({'errorcode': 40, 'evName': 'on_msg_in_e_4*', 'mesNumber': messnum})
        currentstate = self.pm.currentFsm.state

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm, self.pm.KnitFsm)
        self.assertEqual(self.pm.currentFsm.state, currentstate)
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list,
                                      'error 4*',
                                      logging.CRITICAL))


if __name__ == '__main__':
    from common.tools_for_log import logconf

    mlog = logconf('__file__', test=True)

    unittest.main()

