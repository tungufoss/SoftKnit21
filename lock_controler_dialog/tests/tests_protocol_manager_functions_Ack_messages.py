#!/usr/bin/env python
# coding:utf-8
"""
  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

Test for class LCProtocolMgr : second part of the tests for the methods

Implements
==========

Documentation
=============

Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""
import unittest
from unittest.mock import Mock

from lock_controler_dialog.protocol_manager import LCProtocolMgr
from lock_controler_dialog.LC_Helpers import LCProtocolHelpers
from lock_controler_dialog.LC_message_tosend import LCMessagetoSend
from common.SoftKnit21Errors import LCManagerError
import json
import logging
from lock_controler_dialog.tests.tests_super_class import Test_super_class


class Test_functions_sent_messages (Test_super_class, unittest.TestCase):

    def setUp(self):

        super().commonsetUp_for_pm()

    def test1_onAck_sent_message_found(self):
        """test timer canceled, next_buffer called"""
        eventmessage = self.dictmessages.get('ma')
        messnum = eventmessage['mesNumber']
        currentmessage = self.dictmessages.get('ml1')
        self.pm.currentMess = LCMessagetoSend(**currentmessage)
        self.pm.currentMess.datadict['mesNumber'] = messnum
        event = {'evName': 'on_msg_in_n', **eventmessage}
        self.pm.nextBuffer = Mock(autospec=LCProtocolMgr.nextBuffer)

        self.pm.onAck(**event)

        self.pm.nextBuffer.assert_called_once()
        self.assertTrue(self.pm.currentMess is None)

    def test2_onAck_sent_message_not_found(self):
        """test timer canceled, log CRITICAL
        nextBuffer not called and keep currentMess"""
        eventmessage = self.dictmessages.get('ma')
        currentmessage = self.dictmessages.get('ml1')
        currentnum = currentmessage['mesNumber']
        self.pm.currentMess = LCMessagetoSend(**currentmessage)
        curmessage = self.pm.currentMess
        event = {'evName': 'on_msg_in_a', **eventmessage}
        self.pm.nextBuffer = Mock(autospec=LCProtocolMgr.nextBuffer)

        self.pm.onAck(**event)

        self.assertEqual(self.pm.currentMess, curmessage)
        (level, lmessage), emptydic = self.mocklog.log.call_args_list[0]
        self.assertEqual(level, logging.CRITICAL)
        self.assertTrue(f'number is {currentnum}' in lmessage)
        self.pm.nextBuffer.assert_not_called()

    def test3_onAck_sent_message_error_in_type(self):
        """test error raised, timer canceled and keep currentMess"""
        eventmessage = self.dictmessages.get('ma')
        currentmessage = self.dictmessages.get('ml1')
        self.pm.currentMess = LCMessagetoSend(**currentmessage)
        message = self.pm.currentMess
        self.pm.currentMess.mtype = 'q'
        event = {'evName': 'on_msg_in_a', **eventmessage}

        with self.assertRaises(LCManagerError):
            self.pm.onAck(**event)

        self.assertEqual(self.pm.currentMess, message)

    def test4_onNack_error_in_type(self):
        """ test error raised, timer canceled and keep currentMess"""
        eventmessage = self.dictmessages.get('ma')
        eventmessage['mtype'] = 'n'
        currentmessage = self.dictmessages.get('ml1')
        self.pm.currentMess = LCMessagetoSend(**currentmessage)
        message = self.pm.currentMess
        self.pm.currentMess.mtype = 'q'
        event = {'evName': 'on_msg_in_n', **eventmessage}

        with self.assertRaises(LCManagerError):
            self.pm.onNack(**event)

        self.assertEqual(self.pm.currentMess, message)

    def test5_onNAck_sent_message_not_found(self):
        """  timer canceled,  and keep currentMess,
        nexBuffer not called logging Critical"""
        eventmessage = self.dictmessages.get('ma')
        eventmessage['mtype'] = 'n'
        currentmessage = self.dictmessages.get('ml1')
        self.pm.currentMess = LCMessagetoSend(**currentmessage)
        message = self.pm.currentMess
        currentnum = message.datadict['mesNumber']
        event = {'evName': 'on_msg_in_n', **eventmessage}
        self.pm.nextBuffer = Mock(autospec=LCProtocolMgr.nextBuffer)

        self.pm.onNack(**event)

        self.assertEqual(self.pm.currentMess, message)
        (level, lmessage), emptydic = self.mocklog.log.call_args_list[0]
        self.assertEqual(level, logging.CRITICAL)
        self.assertTrue(f'number is {currentnum}' in lmessage)
        self.pm.nextBuffer.assert_not_called()

    def test6_onNAck_sent_message_found(self):
        """  timer canceled,  and currentMess remains,
        nextBuffer not called , repeat_write called, log critical"""
        eventmessage = self.dictmessages.get('ma')
        eventmessage['mtype'] = 'n'
        messnum = eventmessage['mesNumber']
        currentmessage = self.dictmessages.get('ml1')
        self.pm.currentMess = LCMessagetoSend(**currentmessage)
        self.pm.currentMess.datadict['mesNumber'] = messnum
        message = self.pm.currentMess
        event = {'evName': 'on_msg_in_n', **eventmessage}
        self.pm.nextBuffer = Mock(autospec=LCProtocolMgr.nextBuffer)

        self.pm.onNack(**event)

        self.pm.nextBuffer.assert_not_called()
        self.assertEqual(self.pm.currentMess, message)
        (LCmessage, timeout), emptydic = self.pm.LC.repeat_write.call_args_list[0]
        self.assertEqual(LCmessage, message)
        self.assertEqual(timeout, LCProtocolHelpers.TIMEOUT_MESSAGE)
        (level, lmessage), emptydic = self.mocklog.log.call_args_list[0]
        self.assertEqual(level, logging.DEBUG)
        self.assertTrue(f'''message {event['mesNumber']}''' in lmessage)

    def test7_create_write_message_(self):
        """test message created is correct, message sent with timeOut
        repeat is the new event to create, og of sent message but not event"""
        event = {'evName': 'normal'}
        timeO = 2

        self.pm.create_write_message('W', timeout=timeO, **event)

        self.assertTrue(isinstance(self.pm.currentMess, LCMessagetoSend))
        self.assertEqual(self.pm.currentMess.mtype, 'W')
        args, kwargs = self.pm.LC.write_message.call_args
        (message, timeout) = args
        self.assertEqual(message, self.pm.currentMess)
        self.assertEqual(timeout, timeO)
        self.assertEqual(self.pm.currentMess.repeat, 0)
        (level, evdic), emptydic = self.mocklog.log.call_args_list[0]
        self.assertEqual(level, logging.DEBUG)
        logdic = json.loads(str(evdic))
        re = {key: value for key, value in logdic.items() if (key, value) in self.pm.currentMess.toDict().items()}
        self.assertEqual(re, self.pm.currentMess.toDict())


if __name__ == '__main__':
    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__file__', test=True)

    unittest.main()
