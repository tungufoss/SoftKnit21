#!/usr/bin/env python
# coding:utf-8
"""
  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

test for functions in LCmodel helpers
==========



Documentation
=============


Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""
from lock_controler_dialog.LC_Helpers import LCProtocolHelpers, LCConfigurations
import unittest


class TestLCHelpersFunctions (unittest.TestCase):
    def setUp(self):
        pass

    def test_get_LClockpos(self):
        data = 50
        res = -40 + 8192
        self.assertEqual(LCProtocolHelpers.get_LClockpos(50), res)
        data = 100
        res = 10 + 8192
        self.assertEqual(LCProtocolHelpers.get_LClockpos(100), res)

    def test_get_SKlockpos(self):
        data = -40 + 8192
        res = 50
        self.assertEqual(LCProtocolHelpers.get_SKlockpos(data), res)
        data = 10 + 8192
        res = 100
        self.assertEqual(LCProtocolHelpers.get_SKlockpos(data), res)

    def test_check_brand(self):
        from lock_controler_dialog.tests.data.data_for_LCMessages_Tests import datadictmessages
        data = datadictmessages.get('mw').get('brand')
        self.assertTrue(LCProtocolHelpers.check_brand(data))
        data = 'toto'
        self.assertEqual(LCProtocolHelpers.check_brand(data), 'toto is not yet supported by Softknit21')

    def test_is_LC(self):

        self.assertFalse(LCConfigurations.is_LC('toto'))
        self.assertTrue(LCConfigurations.is_LC('this is Arduino'))

    def test_is_hardware_OK(self):
        brand = 'PassapE6000 '
        values = {'Vcc': 5, 'V15': 14}
        valuesNOK1 = {'Vcc': 0, 'V15': 14}
        valuesNOK2 = {'Vcc': 5, 'V15': 10}
        self.assertTrue(LCConfigurations.is_hardware_OK(brand, **values))
        self.assertFalse(LCConfigurations.is_hardware_OK(brand, **valuesNOK1))
        self.assertFalse(LCConfigurations.is_hardware_OK(brand, **valuesNOK2))


if __name__ == '__main__':
    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__file__', test=True)
    unittest.main()

