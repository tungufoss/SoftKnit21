#!/usr/bin/env python
# coding:utf-8
"""
  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

data for LCMessages tests

Implements
==========



Documentation
=============


Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""

from lock_controler_dialog.LC_Helpers import LCProtocolHelpers

dataformessages = {
    'ma': {'mtype': 'a', 'lenpayload': 1, 'payload': [b'\x14'], 'checksum': b'\x76'},
    'md': {'mtype': 'd', 'lenpayload': 4, 'payload': [b'\x00', b'\x37', b'\x01', b'\x05'], 'checksum': b'\x25'},
    #payload in decimal [0,55,1, 5]
    'me': {'mtype': 'e', 'lenpayload': 2, 'payload': [b'\x14', b'\x7b', ], 'checksum': b'\x76'},
    'mA': {'mtype': 'A', 'lenpayload': -1, 'payload': []},
    'mR': {'mtype': 'R', 'lenpayload': -1, 'payload': []},
    'mp': {'mtype': 'p', 'lenpayload': 2, 'payload': [b'\x40', b'\x52'], 'checksum': b'\x04'},
    'ms': {'mtype': 's', 'lenpayload': 1, 'payload': [b'\x33'], 'checksum': b'\x27'},
    # payload in decimal :[51]
    'mw': {'mtype': 'w', 'lenpayload': 19, 'payload': [b'\x50', b'\x61', b'\x73', b'\x73', b'\x61',
                                                       b'\x70', b'\x45', b'\x36', b'\x30', b'\x30',
                                                       b'\x30', b'\x20', b'\x4c', b'\x43', b'\x31',
                                                       b'\x35', b'\x32', b'\x30', b'\x31'],
           # payload in decimal :[80, 97, 115, 115, 97, 112, 69, 54, 48, 48, 48, 32, 76, 67, 49, 53, 50, 48, 49]
           'checksum': b'\x25'},
    'mp2': {'mtype': 'p', 'lenpayload': 2, 'payload': [b'\x3f', b'\x02'], 'checksum': b'\x33'},
    # payload in decimal [63, 2]
    'me2': {'mtype': 'e', 'lenpayload': 1, 'payload': [b'\x0a'], 'checksum': b'\x70'},
    # payload in decimal [10]

    'ml1': {'mtype': 'l', 'lenpayload': 8, 'mesNumber': 1, 'payload': [b'\x01', b'?', b'D', b'?', b'X', b'D', b'1', b'\x0c'],
            'checksum': b'\x10'},
    # payload in decimal : 1,63,68,63,88,68,49,12, checksum in hexa: x\10
    # payload in hexadecimal : x\01,x\3f,x\44,x\3f,x\58,x\44,x\31,x\0c


    'mr2': {'mtype': 'r', 'lenpayload': 9, 'mesNumber': 2,
            'payload': [b'\x02', b'?', b'&', b'?', b'<', b'\x02', b'\x08', b'!', b'B'],
            'checksum': b'J'},
    # payload in decimal : 2,63,38,63,60,2,8,33,66, checksum in hexa: x\4a
    # payload in hexadecimal : x\02,x\3f,x\26,x\3f,x\3c,x\02,x\08,x\21,x\42


    'mr3': {'mtype': 'r', 'lenpayload': 31, 'mesNumber': 3,
            'payload': [b'\x03', b'?', b'&', b'@', b'X', b'\x08', b'\x10', b' ', b'A', b'A', b'\x02', b'\x04', b'\x0c',
                        b'\x08', b'\x10', b' ', b'`', b'A', b'\x02', b'\x06', b'\x04', b'\x08', b'\x10', b'0', b' ', b'A',
                        b'\x03', b'\x02', b'\x04', b'\x08', b'\x18'],
            'checksum': b'\x14'},
    # payload in decimal : 3,63,38,64,88,8,16,32,65,65,2,4,12,8,16,32,96,65,2,6,4,8,16,48,32,65,3,2,4,8,24, checksum in hexa: x\14
    # payload in hexadecimal : x\03,x\3f,x\26,x\40,x\59,x\08,x\10,x\20,x\41,x\41,x\02,x\04,x\0c,x\08,x\10,x\20,x\60,x\41,x\02,x\06,x\04,x\08,x\10,x\30,x\20,x\41,x\03,x\02,x\04,x\08,x\18


    'ml4': {'mtype': 'l', 'lenpayload': 11, 'mesNumber': 4, 'payload': [b'\x04', b'@', b'\x14', b'@', b'<', b'@', b'\x10',
                                                                        b'\x04', b'\x01', b'\x00', b' '],
            'checksum': b'@'},
    # payload in decimal : 4,64,20,64,60,64,16,4,1,0,32, checksum in hexa: x\40
    # payload in hexadecimal : x\04,x\40,x\14,x\40,x\3c,x\40,x\10,x\04,x\01,x\00,x\20


    'ml5': {'mtype': 'l', 'lenpayload': 17, 'mesNumber': 5, 'payload': [b'\x05', b'?', b'&', b'?', b's', b'L', b'f', b'3',
                                                                        b'\x19', b'L', b'f', b'3', b'\x19', b'L', b'f', b'3',
                                                                        b'\x00'],
            'checksum': b'z'},
    # payload in decimal :  5,63,38,63,115,76,102,51,25,76,102,51,25,76,102,51,0, checksum in hexa: x\7a
    # payload in hexadecimal : x\05,x\3f,x\26,x\3f,x\73,x\4c,x\66,x\33,x\19,x\4c,x\66,x\33,x\19,x\4c,x\66,x\33,x\00


    'mr6': {'mtype': 'r', 'lenpayload': 9, 'mesNumber': 6, 'payload': [b'\x06', b'?', b"'", b'?', b'<', b'\x01', b'\x00',
                                                                       b'@', b' '],
            'checksum': b'C'},
    # payload in decimal : 6,63,39,63,60,1,0,64,32, checksum in hexa: x\43
    # payload in hexadecimal : x\06,x\3f,x\27,x\3f,x\3c,x\01,x\00,x\40,x\20
    'mt': {'mtype': 't', 'lenpayload': 10, 'payload': [b'\x01', b'\x14', b'l', b'?', b'D', b'?',
                                                       b'X', b'D', b'1', b'\x0c'],
           # payload in decimal : [1, 20, 108, 63, 68, 63, 88, 68, 49, 12] checksum in demcimal : 26
           'checksum': b'\x1a'},


}


datadictmessages = {'ma': {'mtype': 'a', 'class': 'normal', 'mesNumber': 20, 'checksum': True},
                    'md': {'mtype': 'd', 'class': 'normal', 'Vcc': 5.5, 'V15': 13.3, 'checksum': True},
                    'me': {'mtype': 'e', 'class': 'normal', 'errorcode': 20, 'mesNumber': 123, 'checksum': True},
                    'mA': {'mtype': 'A', 'class': 'normal', 'checksum': None},
                    'mR': {'mtype': 'R', 'class': 'normal', 'checksum': None},
                    'mp': {'mtype': 'p', 'class': 'test', 'lockpos': 172, 'checksum': True},
                    # position out of range
                    'mp2': {'mtype': 'p', 'class': 'test', 'lockpos': -126, 'checksum': True},
                    'ms': {'mtype': 's', 'class': 'test', 'PrevDir': LCProtocolHelpers.RTL,
                           'PrevCsense': 1,
                           'PrevCref': 0,
                           'Dir': LCProtocolHelpers.LTR,
                           'CSense': 1,
                           'Cref': 1,
                           'checksum': True},
                    'mw': {'mtype': 'w', 'class': 'normal', 'brand': 'PassapE6000 ',
                           'hard': 'LC15',
                           'soft': '201',
                           'checksum': True},
                    'me2': {'mtype': 'e', 'class': 'normal', 'errorcode': 10, 'mesNumber': None, 'checksum': True},

                    'ml1': {'mtype': 'l', 'class': 'normal', 'mesNumber': 1, 'leftmost': 30, 'rightmost': 50,
                            'LTRBuffer': '100010001100010001100', 'checksum': True},
                    'mr2': {'mtype': 'r', 'class': 'normal', 'mesNumber': 2, 'leftmost': 0, 'rightmost': 22,
                            'RTLBuffer': '10000100001000011000010', 'checksum': True},
                    'mr3': {'mtype': 'r', 'class': 'normal', 'mesNumber': 3, 'leftmost': 0, 'rightmost': 178,
                            'RTLBuffer': '10000010000010000010000011000001000001000001000001100000100000100000100000110000010000010000010000011000001000001000001000001100000100000100000100000110000010000010000010000011000',
                            'checksum': True},
                    'ml4': {'mtype': 'l', 'class': 'normal', 'mesNumber': 4, 'leftmost': 110, 'rightmost': 150,
                            'LTRBuffer': '10000000010000000010000000010000000010000', 'checksum': True},
                    'ml5': {'mtype': 'l', 'class': 'normal', 'mesNumber': 5, 'leftmost': 0, 'rightmost': 77,
                            'LTRBuffer': '100110011001100110011001100110011001100110011001100110011001100110011001100110',
                            'checksum': True},
                    'mr6': {'mtype': 'r', 'class': 'normal', 'mesNumber': 6, 'leftmost': 1, 'rightmost': 22,
                            'RTLBuffer': '1000000010000000100000', 'checksum': True},
                    'mt': {'mtype': 't', 'class': 'test', 'buffindex': 1, 'mesNumber': 20, 'bufftype': 'l',
                           'leftmost': 30, 'rightmost': 50,
                           'LTRBuffer': '100010001100010001100',
                           'checksum': True},
                    'mt2': {'mtype': 't', 'class': 'test', 'buffindex': 1, 'mesNumber': 20, 'bufftype': 'l',
                            'leftmost': 0, 'rightmost': 22,
                            'LTRBuffer': '10000100001000011000010',
                            'checksum': True}
                    }



