# Lock Controler Dialog

This project is about the dialog with the Lock Controler. This python software exchanges messages with the electronic LockControler.
This python software analyses the messages received from the lock controler, sends messages to the lock controler.
The software implements a protocol which is described by 2 types of documents: a document describing the content of the messages and several finite 
state machines diagram . All these documents are included into the documentation section.
The python software assumes that the lock controler software, ran on the arduino,  behaves in a certain manner. 
The expectations from this software are described in the Finite State Machines of the Lock Controler included into the documentation.

## Project status
all modules in  the project have been thouroughly unitary tested and all tests are included in the project.
More tests need to be written for integration and testing the protocol. See the testing strategy document.
