# coding:utf-8
"""
  This file is part of SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

manages communication with the lock controler

Implements
==========

 - B{Lock Controler Manager}

Documentation
=============

    sends and receive messages from the lock controler

Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""

import serial
import serial.tools.list_ports
from threading import Timer
from common.SoftKnit21Errors import LockControlerError
from gettext import gettext as _
from lock_controler_dialog.LC_messages_reader import LCMesReader
from common.tools_for_log import logconf, StrucLogMessage
import logging


mlog = logging.getLogger(__name__)


class LCMesManager:

    """  create the lockReader and sends the messages to the LC
    Create the timer for timeout management, handles timer expiration 
    repeat of messages, and event to create in case of maximum retries   
    
    @devices : possible devices identifiecd for LC
    @type : list
    
    @fsm : protocol manager reference
    @type: protocol manager instance
    
    @serialport : connection used for dialog with the LC
    @type : Serial class of PySerial
    
    @portparams : parameters for the serial port
    @type:  dictionnary
    
    @lockReader : lockReader managing messages received from the LC
    @type : LockReader instance (and separate thread) 
    
    @queue : the events queue used by lockReader to write events and by protocol manager to read the events
    @type : an instance of a python simple QueueObject 
    """
    DEFAULT_SERIALPARAMS = {
        'port': '',
        'baudrate': 9600,
        'bytesize': serial.EIGHTBITS,
        'parity': serial.PARITY_NONE,
        'stopbits': serial.STOPBITS_ONE,
        'timeout': 0,
        'xonxoff': False,
        'rtscts': False,
        'write_timeout': None,
        'inter_byte_timeout': None,
        'exclusive': False,
    }

    def _find_device(self):
        """ find the device on which the lock controler is connected
        Note : as long as devices do not contain an id it is necessary to recognize the device from the
        chip in the Lock Controler
        if no device found retries one time, after asking 
       the user to unconnect usb cable and reconnect it"""
        from lock_controler_dialog.LC_Helpers import LCConfigurations
        recognized_devices = []
        retries = 0
        ports = serial.tools.list_ports.comports()
        devices_dic = [port.__dict__ for port in ports]
        while not recognized_devices and retries <= 1:
            recognized_devices = [dic.get('device', None) for dic in devices_dic
                                  if LCConfigurations.is_LC(dic.get('description'))]
            if not recognized_devices:
                if retries == 1:
                    self.pm.call_presenter('request',
                                           _(f'again no lock controler found : unconnect usb cable and '
                                             f'check that no other program is using the lock controler',
                                             blocking=True))
                    retries += 1
                elif retries == 0:
                    self.pm.call_presenter('request',
                                           _(f'no lock controler found; please connect the lock controler to the USB port'),
                                           blocking=True)
                    retries += 1

        if not recognized_devices:
            raise LockControlerError(
                _(f'no lock controler found after several retries'))

        return recognized_devices

    def _choose_previous_device(self, devices):
        """if several devices were found choose the one that was used in previous configuration 
        else return the first one"""
        from common.configuration_manager import get_configuration, LockControlerConf
        if not isinstance(devices, list):
            return devices
        # get lockcontroler configuration
        conf = get_configuration()
        conf = conf.lockControlerConf
        previousport = conf.get_element('port')

        if (previousport is not None and
                previousport in devices):
                return previousport
        return devices[0]

    def __init__(self, protocol_manager, queue):
        """Constructor of lock controler"""
        self.pm = protocol_manager
        self.queue = queue
        self.devices = self._find_device()
        if isinstance(self.devices, list):
            if len(self.devices) > 1:
                port = self._choose_previous_device(self.devices)
            else:
                port = self.devices[0]
        else:
            port = self.devices
        self.serialport = self.start_serial(port)

    def start_serial(self, port):
        mlog.log(logging.DEBUG, f'##########restart Lockcontroler  on port {port} ################')
        self._portparams = self.DEFAULT_SERIALPARAMS
        self._portparams.update({'port': port})
        serialport = serial.Serial(**self._portparams)
        serialport.reset_output_buffer()

        # creates the Lock reader thread to read messages
        self.lockReader = LCMesReader(serialport, self.queue)
        # start is a method of class thread
        self.lockReader.start()
        return serialport

    def retry_LC_connection(self):
        """ if possible tries a new device when LC connection failed"""
        self.lockReader.stop_thread()
        self.lockReader = None
        previousSerialPort = self.serialport
        previousSerialPort.close()
        previousPort = self._portparams.get('port')

        if isinstance(self.devices, list):
            if len(self.devices) >= 1:
                i = self.devices.index(previousPort)
                self.devices.pop(i)
                if self.devices:
                    previousPort = self.devices[0]
                    self._portparams.update({'port': previousPort})
                    self.serialport = self.start_serial(previousPort)
                    return
        raise LockControlerError(
            _(f'no lock controler found after several retries'))

    @property
    def portparams(self):
        return self._portparams

    def _write_bytes(self, LCmessage, timeout):
        """ write LCmessage on serial port with or without timeout"""

        s = 0
        while s < len(LCmessage.bytestosend):
            r = self.serialport.write(LCmessage.bytestosend[s:])
            s = s + r
        LCmessage.repeat += 1
        if timeout != None:
            LCmessage.timer = Timer(timeout, self.repeat_write, args=[LCmessage, timeout])
            LCmessage.timer.start()
        else:
            LCmessage.timer = None

    def repeat_write(self, LCmessage, timeout=None):
        """ call back for time out expires
        resend the message if repeat attribute of the current message is an int
        and lower to MAXREPEAT
        triggers an event if repeat attribute of the current message is a string or higher than MAXREPAET 
        """
        from lock_controler_dialog.LC_Helpers import LCProtocolHelpers
        if isinstance(LCmessage.repeat, int) and LCmessage.repeat < LCProtocolHelpers.MAXREPEAT:
            mlog.log(logging.CRITICAL, f'timeout expired for sending of message ')
            mlog.log(logging.CRITICAL, StrucLogMessage(**LCmessage.toDict()))
            LCmessage.repeat += 1
            self._write_bytes(LCmessage, timeout)
        elif isinstance(LCmessage.repeat, int) and LCmessage.repeat >= LCProtocolHelpers.MAXREPEAT:
            event = {'evName': 'on_max_repeat',
                     **LCmessage.toDict()}
            self.pm.trigger(**event)
        elif isinstance(LCmessage.repeat, str):
            event = {'evName': LCmessage.repeat,
                     **LCmessage.toDict()}
            self.pm.trigger(**event)
        else:
            raise LockControlerError(f'value Error in LCmessage repeat {LCmessage.repeat}; {LCmessage.toDict()}')

    def write_message(self, LCmessage, timeout=None):
        """ sends a message to the Lock Controler

        @LCmessage, to send
        @type : instance of LCMessagetoSend

        @param timeout : timeout to start when sending message
        @type : None or value for the time out"""

        LCmessage.bytestosend = [ord(LCmessage.mtype) + 128,
                                 LCmessage.lenpayload,
                                 *LCmessage.payload,
                                 LCmessage.checksum] if LCmessage.mtype.islower() else [ord(LCmessage.mtype) + 128]
        self._write_bytes(LCmessage, timeout)


if __name__ == '__main__':
    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__name__', test=True)

    from pathlib import Path
    current = Path(__file__)
    tests_dir = Path(current.parent, 'tests')
    loader = unittest.TestLoader()
    tests_suite = loader.discover(tests_dir, pattern='tests_' + current.stem + '*.py')
    testRunner = unittest.runner.TextTestRunner()
    testRunner.run(tests_suite)



