#!/usr/bin/env python
# coding:utf-8
"""
  This file is part of SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

LC messages received from the lock controler and their payload

Implements
==========

 - B{Lock Controler Manager}

Documentation
=============

    LC messages as received by the lock controler
Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""


from common.SoftKnit21Errors import LCMessageError
from common.tools_for_log import StrucLogMessage
from lock_controler_dialog.LC_Helpers import LCProtocolHelpers
from lock_controler_dialog.LC_message_abstract import LCMessage, Payload
import logging

mlog = logging.getLogger(__name__)


class LCMessageReceived (LCMessage):
    """ contains a received message
    @att mtype : type of the message
    @type : ascii character

    @att lenpayload : length of the payload of the message
    @type : int (-1 for a single byte message)

    @att payload : payload of the message
    @type : list of integers

    @att checksum : checked checksum
    @type : True or False

    """

    def set_checksum(self, value):
        """ chechs the checksum for the message
        
        @param value : value to check the checksum against
        @type  value : bytes or int"""
        # checksum is modulo 128
        import sys

        if self.mtype.islower():
            if self.lenpayload == None:
                raise LCMessageError(f'type : {self.mtype} must have a lenpayload')
            check = ord(self.mtype) + self.lenpayload + sum(self.payload)
            if isinstance(value, bytes):
                value = int.from_bytes(value, sys.byteorder)
            self._checksum = (check % 128 == value)
            return
        else:
            # a singlebyte message has no checksum
            raise LCMessageError(f'type : {self.mtype} checksum proposed {value } : no checksum for single byte message')

    def __init__(self, mtype, lenpayload=None):
        """ create an instance of LCMessageReceived  on the fly (for received messages)

        @param mtype : mtype of the message
        @type : ascii character

        @param lenpayload : length of the payload of the message
        @type : integer or None"""
        self.datadict = {}
        if mtype is None:
            LCMessageError(f'{mtype} must be present ')
            return
        else:
            self.mtype = mtype
        self._payload = Payload()
        self.lenpayload = None
        self._checksum = None
        # index used to create the dictionnary from the payload
        self.payloadindex = 0
        if self.mtype.isupper():
            self.lenpayload = -1
            return
        if self.mtype.islower():
            if isinstance(lenpayload, int):
                self.lenpayload = lenpayload
                return
            elif lenpayload is not None:
                raise LCMessageError(f'{lenpayload} must be None or integer')
            return

    def isValid(self):
        """ checks the validity of the message"""
        if (not isinstance(self.mtype, str) or
                not self.mtype in LCProtocolHelpers.rMessagesDef.keys()
                or self.lenpayload is None):
            return False
        elif self.mtype.isupper():
            return (self.lenpayload == -1
                    and len(self.payload) == 0
                    and self.checksum == None
                    )
        elif self.mtype.islower():
            return (self.lenpayload >= 1 and
                    isinstance(self.payload, list) and
                    len(self.payload) == self.lenpayload and
                    isinstance(self.checksum, bool)
                    )
        else:
            return False

    @staticmethod
    def _decode2bytes(bytes2):
            """ decode 2 bytes representing a single value
            right most bit of byte1 bealongs to byte2"""
            if (isinstance(bytes2, list)
                    and len(bytes2) == 2
                    and isinstance(bytes2[0], int)
                    and isinstance(bytes2[1], int)
                    and bytes2[0] < 256
                    and bytes2[1] < 256
                ):
                # least significant bit of byte1 is to put in bytes2
                t = bytes2[0] % 2
                lowbyte = bytes2[1] + t * 128
                highbyte = (bytes2[0] >> 1)
                res = highbyte * 256 + lowbyte
                return res
            else:
                raise LCMessageError(f'in decode2bytes : {bytes2} cannot be decoded')

    @staticmethod
    def _decodebits(byte1, bits):
        """ decode a byte containing several individual bits
        returns the list of individual values the first one
        being the value of the list significant bit"""
        if (isinstance(byte1, int) and isinstance(bits, int) and
                byte1 < 128 and byte1 > 0):
            res = []
            for i in range(0, bits):
                res.append(byte1 % 2)
                byte1 = byte1 >> 1
            res.reverse()
            return res
        else:
            raise LCMessageError(f'incorrect value {byte1} for _decodebits')

    @staticmethod
    def _decodebyte(byte):
        """ return the byte, length is not used"""

        return byte

    @staticmethod
    def _decodeRTLBuff(ints):
        return LCMessageReceived._decodeLTRBuff(ints)

    @staticmethod
    def _decodeLTRBuff(ints):
        char = []
        masks = [64, 32, 16, 8, 4, 2, 1]
        for i in range(0, len(ints), 1):
            num = ints[i]
            char.extend([str(1 * (mask & num != 0)) for mask in masks])
        res = ''.join(char)
        return res

    def _getValue(self, vtype=None, length=None, extra=None):
        """builds the dictionnary entry for a value

        @param vtype : type of the value
        @type : string, amongst the keys of the mesvaluetypes dict in common.LCConstants

        @param length : for certain types the length of the value
        @type : integer

        @param : an extra function that is to be called after getting the value to get the final value
        @type : string"""

        if self.payloadindex < self.lenpayload:
            i = self.payloadindex
            if vtype == 'byte':
                value = self._decodebyte(self.payload[i])
                self.payloadindex += 1
            elif vtype == '2bytes':
                value = self._decode2bytes(self.payload[i: i + 2])
                self.payloadindex += 2
            elif vtype == 'strtype':
                value = self.payload[i: i + length]
                value = ''.join((chr(e) for e in value))
                self.payloadindex += length
            elif vtype == 'bits':
                value = self._decodebits(self.payload[i], length)
                self.payloadindex += 1
            elif vtype == 'bool':
                value = value
                self.payloadindex += 1
            elif vtype == 'LTRBuff':
                value = self._decodeLTRBuff(self.payload[i: i + length])
                self.payloadindex += length
            elif vtype == 'RTLBuff':
                value = self._decodeRTLBuff(self.payload[i: i + length])
                self.payloadindex += length
            else:
                LCMessageError(f'{value} has no type definition', self)
            if extra != None:
                value = self._extra(extra, value)
                if value is None:
                    LCMessageError(f'invalid message value : '
                                   f'{value} does not comply with {extra} function ')

            return value
        # the value is not present
        else:
            return None

    def createLCmesDict(self):
        """ creates LCmessage.datadict  containing the messages values
        returns LCmessage.datadict"""
        # with mtype gets the dictionnary describing the values of the message
        coding = LCProtocolHelpers.rMessagesDef.get(self.mtype, None)
        if coding == None or not self.isValid():
            # ignore invalid messages
            raise LCMessageError(f'invalid message  mtype : {self.mtype} '
                                 f'payload : {self.payload} '
                                 f'lenpayload : {self.lenpayload} ')
        self.datadict.update({'class': coding.get('class')})

        # values is the list of values
        values = coding.get('values')
        if values == None:
            # single byte message, done
            return self.datadict
        for value in values:
            kwargs = LCProtocolHelpers.mesTypesDef.get(value, None)
            if kwargs == None:
                raise LCMessageError(f'{value} has no type definition')
            extra = kwargs.get('extra')
            if extra is not None and 'receive' in extra.keys():
                    rec = extra.get('receive')
                    kwargs.update({'extra': rec})
            if value == 'RTLBuffer' or value == 'LTRBuffer':
                left = self.datadict.get('leftmost')
                right = self.datadict.get('rightmost')
                positions = right - left + 1
                #kwargs['length'] = positions
                extrabits = 7 - (positions % 7) if (positions % 7 != 0) else 0
                length = int(positions / 7) + 1 * (positions % 7 != 0)
                kwargs['length'] = length
                if value == 'RTLBuffer':
                    sliceobjet = slice(extrabits, None, None)
                elif value == 'LTRBuffer':
                    sliceobjet = slice(0, positions)
                kwargs['extra'] = {'sliceO': sliceobjet}
            v = self._getValue(**kwargs)
            if v is None and self.mtype == 'e':
                # an optional message number is not present
                    errorcode = self.datadict.get('errorcode')
                    if errorcode in LCProtocolHelpers.eCodesWithMesNum:
                        raise LCMessageError(f'error message {errorcode} should have message number')
            if isinstance(v, dict):
                self.datadict.update(v)
            elif isinstance(v, list) or isinstance(v, tuple):
                self.datadict.update({str(value): v[0]})
            else:
                self.datadict.update({str(value): v})
        return self.datadict

    @staticmethod
    def eventName(LCmtype, LCclass, errorcode):
        """ returns the name or the event"""
        if LCclass == 'test':
            name = 'on_msg_in_test'
        else:
            name = 'on_msg_in_' + LCmtype
        if LCmtype == 'e' and errorcode is not None:
            code = [LCProtocolHelpers.eventCodes[index] for index, value
                    in enumerate(LCProtocolHelpers.alleCodes) if value == errorcode]
            name = 'on_msg_in_' + LCmtype + '_' + code[0]
        return name

    def createEvent(self):
        """ returns an event containing
        evname , LCmessage datadict , mtype and checksum"""

        self.createLCmesDict()
        if self.checksum == False:
            raise LCMessageError(f'false Checksum in message')
        name = self.eventName(self.mtype, self.datadict['class'], self.datadict.get('errorcode'))

        newdic = self.toDict()
        newdic.update({'evName': name})
        return newdic

    def toDict(self, both=False):
        """ returns a dictionnary of the LCMessage including checksum and mtype
        
        @param both : if true both payload and dict to be included"
        @both type : boolean"""
        if len(self.datadict) == 0 or both == True:

            res = {'mtype': self.mtype,
                   'lenpayload': self.lenpayload,
                   'payload': self.payload,
                   'checksum': self.checksum}
        else:
            res = {'mtype': self.mtype,
                   'checksum': self.checksum,
                   **self.datadict}
        if both == True:
            res.update({**self.datadict})
        return res


def create_from_dic(dic):
    """ create a received message from a dictionnary containing
    payload, lenpaylod, mtype, checksum"""
    mtype = dic.get('mtype')
    lenpay = dic.get('lenpayload')
    res = LCMessageReceived(mtype, lenpay)
    res.payload.add(dic.get('payload'))
    res.set_checksum(dic.get('checksum'))
    return res


if __name__ == '__main__':

    
    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__name__', test=True)

    from pathlib import Path
    current = Path(__file__)
    tests_dir = Path(current.parent, 'tests')
    loader = unittest.TestLoader()
    tests_suite = loader.discover(tests_dir, pattern='tests_' + current.stem + '*.py')
    testRunner = unittest.runner.TextTestRunner()
    testRunner.run(tests_suite)    
    
    


